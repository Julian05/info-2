#include "esfera.h"
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsRectItem>
#include <QGraphicsItem>
#include <QPainter>

esfera::esfera(float r, float _x, float _y){
    x=_x;
    y=_y;
    radio=r;
}
QRectF esfera::boundingRect() const
{
    return QRectF(x,y,2*radio,2*radio);
}
void esfera::paint(QPainter *painter , const QStyleOptionGraphicsItem *option, QWidget *widget){
    painter ->setBrush(Qt::lightGray);
    painter ->drawRect((boundingRect()));
}

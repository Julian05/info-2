#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene=new QGraphicsScene(this);
    scene->setSceneRect(0,0,400,400);
    ui->graphicsView->setScene(scene);
    Esfera[0]= new esfera(10,190,85);
    scene->addItem(Esfera[0]);
    Esfera[1]= new esfera(20,280,180 );
    scene->addItem(Esfera[1]);
    Esfera[2]= new esfera(30,175,270);
    scene->addItem(Esfera[2]);
    Esfera[3]= new esfera(40,70,150);
    scene->addItem(Esfera[3]);
    Esfera[4]= new esfera(5,195,195);
    scene->addItem(Esfera[4]);


}

MainWindow::~MainWindow()
{
    delete ui;
}

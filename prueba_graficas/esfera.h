#ifndef ESFERA_H
#define ESFERA_H
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsRectItem>
#include <QGraphicsItem>
#include <QPainter>


class esfera:public QGraphicsItem
{
public:
    esfera(float r, float _x, float _y);
    QRectF boundingRect() const;
    void paint(QPainter *painter , const QStyleOptionGraphicsItem *option, QWidget *widget);
private:
    float radio;
    float x;
    float y;

};

#endif // ESFERA_H

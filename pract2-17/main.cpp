#include <iostream>
using namespace std;
/*un programa que reciba un número y halle la suma de todos los números amigables menores al número ingresado.*/
int validar_num();/*funcion que verifica si es dato ingresado es un numero*/
int main()

{
    int numeroIn,sumaDiv=0, sumaAmi=0;
    cout << "Ingrese un numero: " << endl;
    numeroIn=validar_num();
    if (numeroIn > 0){
        for (int primerNum= 1; primerNum <= numeroIn; primerNum++){/*Ciclo toma el primer valor, se comparara con el contador del segundo ciclo*/
            for (int segundoNum = 1; segundoNum <= numeroIn; segundoNum++ ){ /* segundo valor que se compara con el contador del primer ciclo*/
                for (int divisores = 1;divisores < segundoNum; divisores++){  /*Ciclo que saca el numero de divisores del numero segundoNumeroAmigable*/
                    if (segundoNum % divisores == 0){ /*se mira si al dividir el numero del primer ciclo por el contador divisores, es un divisor*/
                        sumaDiv+=divisores; /*Si es divisor, se agrega a la suma de divisores del numero dado*/
                    }
                }
                if (sumaDiv == primerNum){/*Se verifica si   la suma de divisores es igual al numero dado, es un numero amigable*/
                    sumaAmi += segundoNum;/*Al ser numero amigo, se agrega a la suma de los numeros amigables*/
                    cout<<"numero 1:  "<<primerNum<<"  numero 2:  "<<segundoNum<<endl;
                    sumaAmi+=primerNum;
                    cout <<"La suma de numeros amigos anteriores : "<< sumaAmi<<endl;
                    sumaAmi=0;
                }
                sumaDiv=0;/*Reinicio de la variable sumaDivisores para sacar la suma de los divisores del siguiente numero*/
            }
        }
    }
    else
        cout <<"Ingrese un dato valido, un numero + "<<endl;
    return 0;
}
int validar_num(){
    bool key=false;
    string num;
            while (key==false){
            cout<<""<<endl;
            cin >> num;
            if(num.length()==false) return  false;
            for(int i =0;i<num.length();i++){
                if(isdigit(num[i])) key=true;
                else {
                    key=false;
                    break;}
            }
            if(key==false) cout<<"intente de nuevo"<<endl;
}
            return atoi(num.c_str());

};





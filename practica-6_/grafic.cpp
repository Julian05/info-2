#include "grafic.h"
#include <QPainter>

rasengangrafic::rasengangrafic(QGraphicsItem* rass):QGraphicsPixmapItem(rass)
{
    ax=0;
    ay=0;
     masa=50;
     radio=5;
     posx=0;
     posy=10;
     e=0.9;
     k=0.01;
     vx=5;
     vy=5;
     v= sqrt(pow(vx,2)+pow(vy,2));
     angulo=atan2(vy,vx);
     g=10;
    dt=0.1;
    QPixmap pixmap;
    setPixmap(QPixmap(":/new/prefix1/bolita.png"));

}

void rasengangrafic::actualizar_a(){
    ax=-(((k*v*pow(radio,2))/masa)*cos(angulo));
    ay=-(((k*v*pow(radio,2))/masa)*sin(angulo))-g;
}
void rasengangrafic::choque(){
        v=-e*v;
        vy=-1*vy*e;
        posy=posy*-1;

}
void rasengangrafic::choque_lateral(){
        v=-e*v;
        vx=-1*vx*e;
        posx=posx*-1;

}
void rasengangrafic::actualizar_pos(){
    vx=vx+ax*dt;
    vy=vy+ay*dt;
    posx=posx+vx*dt+((ax*pow(dt,2))/2);
    posy=posy+vy*dt+((ay*pow(dt,2))/2);
}
void rasengangrafic::actualizar_angulo(){
    angulo=atan2(vy,vx);
}
void rasengangrafic::actualizar_v(){
    double aux_v=v;
    v= sqrt(pow(vx,2)+pow(vy,2));
    e=-1*(v/aux_v);
}

double rasengangrafic::get_x(){
    return  posx;
}
double rasengangrafic::get_y(){
    return  posy ;
}

void rasengangrafic::mover(){
    actualizar_a();
    actualizar_pos();
    setPos(x()+(posx),y()-posy);
    actualizar_v();
    actualizar_angulo();

}
rasengangrafic::~rasengangrafic(){

}

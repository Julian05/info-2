 #ifndef RASENGANGRAFIC_H
#define RASENGANGRAFIC_H
#include "ball.h"
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QPainter>


class rasengangrafic:public QObject,
                     public QGraphicsPixmapItem,
                     public rasengan
{
    Q_OBJECT

private:
    double masa;
    double radio;
    double posx;
    double posy;
    double e;
    double k;
    double vx;
    double vy;
    double v;
    double angulo;
   double g;
    double dt;
    double ax;
    double ay;
    double PI= 3.14159265;

public:
    rasengangrafic(QGraphicsItem *rass = nullptr);
    void actualizar_a();
    void choque();
    void choque_lateral();
    void actualizar_pos();
    void actualizar_v();
    void actualizar_angulo();
    double get_x();
    double get_y();

    void mover();
    ~rasengangrafic();

};

#endif // RASENGANGRAFIC_H

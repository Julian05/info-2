#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QTimer>
#include <QFile>
#include "grafic.h"
#include "ball.h"
#include <QKeyEvent>
#include <QFileDialog>
#include <QGraphicsView>




namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent =nullptr );
    ~MainWindow();

public slots:
    void mousePressEvent(QMouseEvent *ev);
    void animar();

private:
     QTimer *timer;
    Ui::MainWindow *ui;
    int h_limit;                //longitud en X del mundo
    int v_limit;
    QGraphicsScene *scene;
    QGraphicsView *view;
    QGraphicsLineItem* l1;
    QGraphicsLineItem* l2;
    QGraphicsLineItem* l3;
    QGraphicsLineItem* l4;
     QList<rasengangrafic*> rasengas;
};

#endif // MAINWINDOW_H

#include "mainventana.h"
#include "ui_mainwindow.h"
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QDialog>

MainWindow::MainWindow(QWidget *parent) :
   QMainWindow(parent),
   ui(new Ui::MainWindow)
{
   ui->setupUi(this);
   h_limit=1000;
   v_limit=500;
   scene=new QGraphicsScene(0,0, h_limit,v_limit);

   ui->View->setScene(scene);
   ui->View->setBackgroundBrush(QImage(":/new/prefix1/konoha.jpg"));

   l1=new QGraphicsLineItem(50,20,900,20);
   l2=new QGraphicsLineItem(50,20,50,450);
   l3=new QGraphicsLineItem(900,20,900,450);
   l4=new QGraphicsLineItem(50,450,900,450);
   scene->addItem(l1);
   scene->addItem(l2);
   scene->addItem(l3);
   scene->addItem(l4);
   timer=new QTimer(this);
   timer->stop();
   connect(timer,SIGNAL(timeout()),this,SLOT(animar()));
   timer->start(20);
}
void MainWindow::mousePressEvent(QMouseEvent *ev)
{
   rasengas.append(new rasengangrafic());
   scene->addItem(rasengas.last());
   rasengas.last()->setPos(ev->x(),ev->y());
}
void MainWindow::animar(){

   for(int i=0; i<rasengas.length();i++)
   {

       rasengas.at(i)->mover();
       if(!rasengas.at(i)->collidingItems().empty())
       {
           rasengas.at(i)->choque();
          //  ui->View->setBackgroundBrush(QImage(":/new/prefix1/konoha.jpg"));
            if(rasengas.at(i)->collidesWithItem(l2)|| rasengas.at(i)->collidesWithItem(l3)){
               rasengas.at(i)->choque_lateral();
                //ui->View->setBackgroundBrush(QImage(":/new/prefix1/konoha2.jpg"));
               }
       }
        }
   }





MainWindow::~MainWindow()
{
   delete scene;
   delete ui;
   delete l1;
   delete l2;
   delete l3;
   delete l4;
   delete timer;
}

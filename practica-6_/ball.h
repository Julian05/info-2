#ifndef RASENGAN_H
#define RASENGAN_H


class rasengan
{
private:
    double masa;
    double radio;
    double posx;
    double posy;
    double e;
    double k;
    double vx;
    double vy;
    double v;
    double angulo;
   double g;
    double dt;
    double ax;
    double ay;
    double PI= 3.14159265;
public:
    rasengan();
    void actualizar_a();
    void choque();
    void choque_lateral();
    void actualizar_pos();
    void actualizar_v();
    void actualizar_angulo();
    double get_x();
    double get_y();
    double get_vx();
    double get_vy();
    void set_x(double xi);
    void set_y(double yi);


};

#endif // RASENGAN_H

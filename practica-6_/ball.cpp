﻿#include "ball.h"
#include "ball.h"
#include <math.h>

rasengan::rasengan()
{
    ax=0;
    ay=0;
     masa=50;
     radio=5;
     posx=0;
     posy=0;
     e=0.9;
     k=0.01;
     vx=5;
     vy=5;
     v= sqrt(pow(vx,2)+pow(vy,2));
     angulo=atan2(vy,vx);
     g=10;
    dt=0.1;
}

void rasengan::actualizar_a(){
    ax=-(((k*v*pow(radio,2))/masa)*cos(angulo));
    ay=-(((k*v*pow(radio,2))/masa)*sin(angulo))-g;
}
void rasengan::choque(){
        v=-e*v;
        vy=-1*vy*e;
        posy=posy*-1;

}
void rasengan::choque_lateral(){
        v=-e*v;
        vx=-1*vx*e;
        posx=posx*-1;

}
void rasengan::actualizar_pos(){
    vx=vx+ax*dt;
    vy=vy+ay*dt;
    posx=posx+vx*dt+((ax*pow(dt,2))/2);
    posy=posy+vy*dt+((ay*pow(dt,2))/2);
}
void rasengan::actualizar_angulo(){
    angulo=atan2(vy,vx);
}
void rasengan::actualizar_v(){
    double aux_v=v;
    v= sqrt(pow(vx,2)+pow(vy,2));
    e=-1*(v/aux_v);
}

double rasengan::get_x(){
    return  posx;
}
double rasengan::get_y(){
    return  posy ;
}

double rasengan::get_vx()
{
    return  vx;
}
double rasengan::get_vy(){
    return  vy ;
}


void rasengan::set_x(double xi)
{
    posx=xi;
}

void rasengan::set_y(double yi)
{
    posy=yi;
}

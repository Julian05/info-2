#include <iostream>
#include<string>
/*un programa que reciba una cadena de caracteres y elimine los caracteres repetidos.*/
using namespace std;

int main()
{
    int posicion=0;
    string dato_entrada;
    bool bandera=true;
    cout<<"escribe algo"<<endl;
    getline(cin,dato_entrada);
    char lista_salida[dato_entrada.length()];/*se crea un arreglo del tamaño de la cadena donde se guardaran los caracteres sin repetir*/
    for(int repetir =0;repetir<dato_entrada.length();repetir++) { /**/
     for(int veces =0;veces<dato_entrada.length();veces++){
         if(dato_entrada[repetir]==lista_salida[veces]){ /*se comprueba si el caracter ya estaba anteriormente o no*/
             bandera=true;/*bandera que nos ayuda para versi esta o no*/
             break;
         }
         else bandera=false;
     }
     if(bandera==false){
         lista_salida[posicion]=dato_entrada[repetir];/*si es la 1 vez que aparece el caracter se guarda*/
         posicion++;
     }
    }
    for(int pos =0;pos<posicion;pos++) cout<<lista_salida[pos]; /*se imprimer caracteres no repetidos*/
    cout<<endl;
}


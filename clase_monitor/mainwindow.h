#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
public slots:
    void Auto();

private slots:
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    int x=0;
    QTimer *timer;
};

#endif // MAINWINDOW_H

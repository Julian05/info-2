#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    timer= new QTimer;
    timer->stop();
    connect(timer,SIGNAL(timeout()),this,SLOT(Auto()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::Auto()
{
    x+=1;
    ui->lcd1->display(x);
}

void MainWindow::on_pushButton_clicked()
{
    if(x==0) ui->label->setText("Start");
    else ui->label->setText("Continue");
    x+=1;
    ui->lcd1->display(x);
    timer->start(500);
}

#include <iostream>
#include <cstring>
#include<fstream>
#include <math.h>



using namespace std;
void metodo(char*,char *);
void leer(const char*,const char*,char,int);
void descodificar_metodo1(const char*,const char*,int,int);
void descodificar_metodo2(const char*,const char*,int,int);
int contar_series(const char*,int);
void backtonormal(int,int,char*);
int contar_letras(const char*);

int main()
{
    char nombrearchio_entrada[30], nombrearchio_salida[30],choose,bits;
    const char *entrada=nombrearchio_entrada,*salida=nombrearchio_salida;
    int limitebits;
    cout<<"escribe el nombre del archivo a descodificar:  "<<endl;
    cin.getline(nombrearchio_entrada,30);
    cout<<"escribe el nombre del archivo de salida:  "<<endl;
    cin.getline(nombrearchio_salida,30);
    metodo(&choose,&bits);
    limitebits=bits-'0';
    leer(nombrearchio_entrada,nombrearchio_salida,choose,limitebits);

}
void leer(const char*nombreentrada,const char*nombresalida,char eleccion,int bitss){
        int code[8],*codificar=code,nseries;
        nseries=contar_series(nombreentrada,bitss);
                if(eleccion=='1'){/*metodo 1*/
                   descodificar_metodo1(nombreentrada,nombresalida,bitss,nseries);
                }
                else if (eleccion=='2') {
                    descodificar_metodo2(nombreentrada,nombresalida,bitss,nseries);

                }

}
int contar_letras(const char* archivosalida){
    ifstream archivo;
    archivo.open(archivosalida,ios::in);
    int series=0,error=0;
    while(archivo.good()){
        for(int veces=0;veces<8;veces++){
            char caracter=archivo.get();
        }
        series++;
    }
    return series;
}

void metodo(char* punte1,char *punte2)
{
    char eleccion,eleccion2[3];
        bool ban=true, ban2=true;
        int ascci,cont=0;
        while(ban || ban2){
            cout << "Ingrese el tipo de metodo(1/2)" << endl;
            cin >> eleccion;
            cout << "Ingrese un numero n ara separar los bits" << endl;
            cin>>eleccion2;
      ascci=(int)eleccion;
      if(ascci>=49 and ascci<=50){
      ban=false;
      *punte1=eleccion;
      }
      else{
       cout<<"ingresa una opcion valida para el metodo"<<endl;
      }
      while(1){
          if (*(eleccion2+cont) == '\0')
              break;
              cont++;
  }
      for(int veces=0;veces<cont;veces++){
         if( isdigit(eleccion2[veces])){
             *(punte2+veces)=eleccion2[veces];
             ban2=false;
         }
         else ban2=true;
      }
      if(ban2==true)cout<<"ingresa una opcion valida para el numero de separacion"<<endl;
    }
    }
int contar_series(const char* nombre, int limite){
    ifstream archivo;
    archivo.open(nombre,ios::in);
    int series=0;
    while(archivo.good()){
        for(int veces=0;veces<limite;veces++){
            char caracter=archivo.get();
        }
        series++;
    }
    return series;
}
void descodificar_metodo1(const char* nombre,const char* nomsalida, int limite,int series_){

    int letras =contar_letras(nombre),quitar0s=0;
    if((limite%2)==0) {
        quitar0s=0;
    }
    else{
        letras=letras-1;
        series_=series_-1;
        quitar0s=abs((series_*limite)-(letras*8));
    }
    ifstream documento;
    documento.open(nombre,ios::in);
    ofstream descodificar;
    int num1s=0,num0s=0,caracter,particion[series_][limite],llenado[series_][limite];
    for(int veces=0;veces<series_;veces++){
        for(int bit=0;bit<limite;bit++){
            char texto2=documento.get();
             caracter=texto2-'0';
            particion[veces][bit]=caracter;
            llenado[veces][bit]=caracter;
        }
    }
    documento.close();
    for(int veces=0;veces<series_;veces++){
        num0s=0;
        num1s=0;
         for(int bit=0;bit<limite;bit++){
             if(veces==0){
               particion[veces][bit]=!particion[veces][bit];
                 if( particion[veces][bit]==1) {
                     num1s++;
                 }
                 if( particion[veces][bit]==0){
                     num0s++;
                 }
             }
             else{
                 if(particion[veces][bit]==1) {
                     num1s++;
                 }
                 if(particion[veces][bit]==0){
                     num0s++;
                 }
             }
         }
         if(veces !=(series_-1)){
         if(num0s==num1s){
             for(int bit=0;bit<limite;bit++){
                particion[veces+1][bit]=!particion[veces+1][bit];
         }

         }
         if(num0s>num1s){
             for(int bit=0;bit<limite;bit++){
                 bit=bit+1;
                    particion[veces+1][bit]=!particion[veces+1][bit];
         }

         }
         if(num1s>num0s){
             for(int bit=0;bit<limite;bit++){
              bit=bit+2;
              if(bit<limite){
                    particion[veces+1][bit]=!particion[veces+1][bit];
              }
         }
         }
         }
    }
    descodificar.open("auxiliar.txt",ios::out);
    for(int veces=0;veces<series_;veces++){
        for(int bit=0;bit<limite;bit++){
            if(quitar0s>0){
                quitar0s=quitar0s-1;
            }
            else{
                descodificar<< particion[veces][bit];
            }
        }
    }
    descodificar.close();
    ofstream final;
    final.open(nomsalida,ios::out);
    int limitemensaje=(series_*limite)/8;

    char mensaje[limitemensaje],*puntemensaje=mensaje;
    backtonormal(limite,series_,puntemensaje);
        cout<<"El mensaje era : "<<endl;
    for(int veces=0;veces<limitemensaje;veces++){
         final<<  mensaje[veces];
         cout<< mensaje[veces];
    }
    cout<<endl;
    final.close();

}
void descodificar_metodo2(const char* nombre,const char* nomsalida, int limite,int series_){
    int letras =contar_letras(nombre),quitar0s=0;
    if((limite%2)==0) {
        quitar0s=0;
    }
    else{
        letras=letras-1;
        series_=series_-1;
        quitar0s=abs((series_*limite)-(letras*8));
    }
    ifstream documento;
    documento.open(nombre,ios::in);
    ofstream descodificar;
    descodificar.open("auxiliar.txt",ios::out);
    int caracter,particion[series_][limite],llenado[series_][limite];
    for(int veces=0;veces<series_;veces++){
        for(int bit=0;bit<limite;bit++){
            char texto2=documento.get();
             caracter=texto2-'0';
            particion[veces][bit]=caracter;
            llenado[veces][bit]=caracter;
        }
    }
    documento.close();
    for(int veces=0;veces<series_;veces++){
        for(int bit=0;bit<limite;bit++){
            if(bit==(limite-1)){
                llenado[veces][bit]=particion[veces][0 ];
        }
            else{
                llenado[veces][bit]=particion[veces][bit+1];
            }
    }
        }
    for(int veces=0;veces<series_;veces++){
        for(int bit=0;bit<limite;bit++){
            if(veces==series_-1 and quitar0s>0){
                quitar0s=quitar0s-1;
            }
            else{
                descodificar<< llenado[veces][bit];
            }
        }
    }
    descodificar.close();
    ofstream final;
    final.open(nomsalida,ios::out);
    int limitemensaje=(series_*limite)/8;

    char mensaje[limitemensaje],*puntemensaje=mensaje;
    backtonormal(limite,series_,puntemensaje);
        cout<<"El mensaje era : "<<endl;
    for(int veces=0;veces<limitemensaje;veces++){
         final<<  mensaje[veces];
         cout<< mensaje[veces];
    }
    cout<<endl;
    final.close();
}
void backtonormal(int limite_, int serie2, char*mensaje){
    serie2=serie2-1;
    ifstream base;
    base.open("auxiliar.txt",ios::in);
    base.seekg(0);
    char texto;
    int asciitonormal[8],auxiliar=0,transicion,conta=0;
    int potencia=7;
        while(base.good()){
        potencia=7;
        auxiliar=0;
       for(int bit=0;bit<8;bit++){
           texto=base.get();
           transicion=texto-'0';
           int paso=transicion*pow(2,potencia);
        asciitonormal[bit]= (paso);
        potencia--;
        auxiliar=auxiliar+asciitonormal[bit];

     }
    *(mensaje+conta)=auxiliar;
       conta++;
        }

}


TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp
INCLUDEPATH += /usr/include/qt/QtSql
INCLUDEPATH += /usr/include/qt/QtWidgets
INCLUDEPATH += /usr/include/qt/QtPrintSupport

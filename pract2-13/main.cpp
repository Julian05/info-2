#include <iostream>

using namespace std;
int num_estrellas(int(*)[8],int,int);
/*una función que reciba un puntero a la matriz de enteros como argumento y que retorne el número
de estrellas encontradas en la imagen.*/

int main()
{
    int fotografia[6][8]={{0,3,4,0,0,0,6,8},{5,13,6,0,0,0,2,3},{2,6,2,7,3,0,10,0},{0,0,4,15,4,1,6,0},{0,0,7,12,6,9,10,4},{5,0,6,10,6,4,8,0}};
    int numfila=6,numcolum=8,estrellas;
    int (*puntero_fotografia)[8]=fotografia;
    /*puntero_fotografia= new int*[6];se reserva memoria para el puntero
    for (int fila=0;fila<numfila;fila++) puntero_fotografia[fila]= new int[8];
    for( int filas=1;filas<(numfila);filas++){ se asignan sus valores a apuntar*/
       /* for(int colum=1;colum<(numcolum);colum++){
            *(*(puntero_fotografia +filas )+colum)=fotografia[filas][colum];
        }
        }*/
    estrellas=num_estrellas(puntero_fotografia,numcolum,numfila);/*se implementa la funcion para contar numero de estrellas*/
    cout<<"en la fotografia hay exactament :"<<estrellas<<"  estrellas"<<endl;

    return 0;
}
int num_estrellas ( int(*puntero_fotografia)[8],int num_colum,int num_fila)
{
    int suma=0,division,can_estrellas=0;
    for( int filas=1;filas<(num_fila-1);filas++){
        suma=0;
        division=0;
        for(int colum=1;colum<(num_colum-1);colum++){ /*se aplica la formula dada para calcular el numero de estrellas con algebra de punteros*/
            suma+= *(*(puntero_fotografia +filas)+colum);
            suma+= *(*(puntero_fotografia +filas)+(colum-1));
            suma+= *(*(puntero_fotografia +filas)+(colum+1));
            suma+= *(*(puntero_fotografia +(filas+1))+colum);
            suma+= *(*(puntero_fotografia +(filas-1))+colum);
            /*suma+=puntero_fotografia[filas][colum];
            suma+=puntero_fotografia[filas][colum-1];
            suma+=puntero_fotografia[filas][colum+1];
            suma+=puntero_fotografia[filas-1][colum];
            suma+=puntero_fotografia[filas+1][colum];*/

            division=suma/5;
            if(division>6) can_estrellas+=1; /*se verifica que si despues de sumarlo y dividir ,si es mayor a 6 , es una estrella*/
            }
        }
    return can_estrellas;
    }


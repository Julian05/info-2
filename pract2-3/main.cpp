#include <iostream>
#include <string>
/* función que compare 2 cadenas de caracteres y retorno un valor lógico verdadero si son
iguales y falso*/

using namespace std;
bool verificar_igualdad(string,string); /* funcion que verifica caracter por caracter de las dos cadenas para verificar si son iguales */
void programa_de_prueba();
int main()
{
    string eleccion;
    string cadena1,cadena2;
    cout<<"¿quiere implementar el programa de prueba? yes/no"<<endl;
    cin>>eleccion;
    cin.ignore();
    if( eleccion == "no" ) {
        cout<<"escribe el primer item a comparar"<<endl;
        getline(cin,cadena1);/* funcion utilizada para captar una cadena de caracteres con espacios */
        cout<<"escribe el segundo item a comparar"<<endl;
        getline(cin,cadena2);
        if(cadena1.length()>cadena2.length())cout<<"Las cadenas no son iguales, diferente tamaño"; /* se verifica si el tamaño de las cadenas es igual*/
            else{
            if( verificar_igualdad(cadena1,cadena2) )cout<<"son iguales"<<endl; /* se implementa la funcion para vereificar caracter por caracter */
            else cout<<"no son iguales"<<endl;
         }
     }

    else programa_de_prueba();

    return 0;
}
bool verificar_igualdad(string cadena1,string cadena2){
    bool key;
    for(int conta=0;conta<cadena1.length();conta++){
        if(cadena1[conta]==cadena2[conta]) key=true;
        else{
            key = false;
            break;}
    }
    return key;

    }
 void programa_de_prueba(){
     string cadena_a,cadena_b,prueba[]={"hola mundo","hola mundo","ciudad de mexico","ciudad de cucuta","tres","dos","uno","uno"};
     for(int veces=0;veces<8;veces+=2){
         cadena_a=prueba[veces];
         cout<<"cadena 1:  "<<cadena_a<<endl;
         cadena_b=prueba[veces+1];
         cout<<"cadena 2:  "<<cadena_b<<endl;
         if(cadena_a.length()>cadena_b.length())cout<<"Las cadenas no son iguales,diferente tamaño"<<endl; /* se verifica si el tamaño de las cadenas es igual*/
             else{
             if( verificar_igualdad(cadena_a,cadena_b) )cout<<"son iguales"<<endl; /* se implementa la funcion para vereificar caracter por caracter */
             else cout<<"no son iguales"<<endl;
         }
     }
 }


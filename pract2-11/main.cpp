#include <iostream>
#include<string>
#include<stdlib.h>
#include<ctype.h>

using namespace std;


/*programa que simula las reservas y cancelaciones en una sala de cine*/
int validar_num();/*funcion que valida datos numericos de entrada*/
void validar_funcion(char*);/*valida si es reserva o cancelacion*/
int validar_fila(char*);/* verifica que este correcto el dato fila*/
void validar_columna(int*);/*verifica que este correcto el dato columna*/
void mostrar_sala(char sala[15][20],int,int);


int main()

{
    string  otra_vez;
    int fila,columna,numcolum=20,numfila=15;
    char  eleccion,sala_cine[15][20],fila2;
    bool repetir= true,validar=false;
    for(int p=0;p<15;p++){ /*se llena la matriz*/
        for(int i=0;i<20;i++) {
            sala_cine[p][i]='-';
        }
    }

    while(repetir==true){

        cout<<"¿quieres realizar una reserva o una cancelacion?(R/C)"<<endl;
        cin>>eleccion;
        validar_funcion(&eleccion);
        if(eleccion=='R'){ /*proceso para reservas*/
            mostrar_sala(sala_cine,numcolum,numfila);
            cout<<"ingrese el numero de la fila (A-O)";
            cin>>fila2;/*se recibe el dato char con una variable char*/
            fila=validar_fila(&fila2);/*se guarda su equivalente numerico en una variable int*/
            cout<<"ingrese el numero de la columna (1-20)"<<endl;
            cin>>columna;
            columna=columna-1;
            validar_columna(&columna);
            sala_cine[fila][columna]='+';/*se cambia el asiento , ahora esta reservado*/
            mostrar_sala(sala_cine,numcolum,numfila);
            cout<<"la reserva se ha realizado con exito";
        }
        else{
            if(eleccion=='C'){ /* proceso para cancelaciones*/
                mostrar_sala(sala_cine,numcolum,numfila);
                cout<<"ingrese el numero de la fila (A-O)";
                cin>>fila2;
                fila=validar_fila(&fila2);
                cout<<"ingrese el numero de la columna (1-20)"<<endl;
                cin>>columna;
                columna=columna-1;
                validar_columna(&columna);
                if((sala_cine[fila][columna])=='-')cout<<"no hay ninguna reserva en este asiento para cancelar";
                else{
                        sala_cine[fila][columna]='-';/*se cambia el asiento , ahora no esta reservado*/
                        cout<<"su reserva se ha realizada con exito"<<endl;
                mostrar_sala(sala_cine,numcolum,numfila);
                }
                }
            }
        cout<<"¿quieres realizar otro proceso(yes/no)?"<<endl;/*se mira si el usuario quiere seguir ejecutando el programa*/
        cin>>otra_vez;
        for(int palabra=0;palabra<otra_vez.length();palabra++) tolower(otra_vez[palabra]);
            while(validar!=true)
            {
            if(otra_vez=="yes"){/*se verifican caracteres de entrada*/
                repetir=true;
                validar=true;
            }
            else{
            if(otra_vez=="no"){/*se verifican caracteres de entrada*/
                repetir=false;
                validar=true;}
            else{
                cout<<"responde con (yes/no)"<<endl;
                cin>>otra_vez;
            }
            }
        }
    }

  return 0;
}
int validar_num(){
    bool key=false;
    string num;
            while (key==false){
            cout<<""<<endl;
            cin >> num;
            if(num.length()==false) return  false;
            for(int i =0;i<num.length();i++){
                if(isdigit(num[i])) key=true;
                else {
                    key=false;
                    break;}
            }
            if(key==false) cout<<"intente de nuevo"<<endl;
}
            return atoi(num.c_str());

};
void validar_funcion(char* dato)
{
 bool bandera=false;
 if(*dato=='R' ||*dato=='C') bandera=true;
 while(bandera==false){
     cout<<"ingrese una respuesta valida"<<endl;
     cin>>*dato;
     if(*dato=='R' ||*dato=='C') bandera=true;
 }
}
int validar_fila(char* car1) /*convierte un caracter a numero oero con una equivalencia de A=0 hasta O=15*/
{
    bool bandera=false;
    while((bandera==false)){
        if(isalpha(*car1)){
        if(((int)*car1>=65 && (int)*car1<=79) ||( (int)*car1>=97 && (int)*car1<=111))/*Estos condicionales,con ayuda de la tabla ascii identifican si el caracter ingresado es una letra o no*/
        {
            if ((int)*car1>=65 && (int)*car1<=79){
                *car1=(int)*car1-65;
                bandera=true;
            }
            else {
                if(((int)*car1>=97 && (int)*car1<=111)){
                *car1=(int)*car1-97;
                bandera=true;
                }
            }
        }
        else {
            cout<<"no es una entrada valida"<<endl;
            cout<<"ingrese una respuesta valida"<<endl;
            cin>>*car1;
        }
        }
        else {
            cout<<"dato invalido,ingrese una letra"<<endl;
            cout<<"ingrese una respuesta valida"<<endl;
            cin>>*car1;
        }
       }
    return *car1;
    }

void validar_columna(int* columna)
{
    bool bandera=false;
    if(*columna>=0 and *columna<=19) bandera=true;/*se mira que el dato ingresado este entre 1-20*/
    else{
    while(bandera==false){
        cout<<"ingrese una respuesta valida";
        cin>>*columna;
        if(*columna>=1 and *columna<=20) bandera=true;
    }
    }

}
void mostrar_sala(char sala[15][20],int  colum,int fila)
{
    cout<<"mira la disponibilidad de asientos '-' disponibles '+' reservados "<<endl;
    for(int j=0;j<fila;j++){ /*se recorre el arreglo para mostrarlo en pantalla*/
        for(int i=0;i<colum;i++){

            cout<< sala[j][i];
        }
        cout<<endl;
    }
}


#include <iostream>
#include <fstream>
void leer(const char*,const char* );
void contar_palabras(const char*,const char*,int*,int*);

using namespace std;

int main()
{
    char nombrearchivo1[30],nombrearchivo2[30];
    const char *nombre1=nombrearchivo1,*nombre2=nombrearchivo2;
    cout << "Escriba el nombre del primer archivo a comparar palabra a palabra:  " << endl;
    cin.getline(nombrearchivo1,30);
    cout << "Escriba el nombre del segundo archivo a comparar palabra a palabra: :  " << endl;
    cin.getline(nombrearchivo2,30);
    leer(nombre1,nombre2);

    return 0;
}
void leer(const char*nombre1,const char*nombre2){
        ifstream archivo1;
        ifstream archivo2;
        archivo1.open(nombre1,ios::in);
        archivo2.open(nombre2,ios::in);
        int numeropalabras1,numeropalabras2,posicion=0, ascii1,ascii2;
        contar_palabras(nombre1,nombre2,&numeropalabras1,&numeropalabras2);
        char texto1[numeropalabras1][100],texto2[numeropalabras2][100];
        bool igualdad=true;
        if(archivo1.is_open() and archivo2.is_open()){
        while(archivo1.good() and archivo2.good()){
            for(int veces=0;veces<numeropalabras1;veces++){
            do{
                 texto1[veces][posicion]=archivo1.get();
                 ascii1=(int)texto1[veces][posicion];
                 posicion++;
            }while (( ascii1!=32) and ( ascii1!=-1) );
                posicion=0;
            }
            posicion=0;
            for(int veces=0;veces<numeropalabras2;veces++){
            do{
                 texto2[veces][posicion]=archivo2.get();
                 ascii2=(int)texto2[veces][posicion];
                 posicion++;
            }while ((ascii2!=32) and (ascii2!=-1) );
              posicion=0;
            }
            posicion=0;
            igualdad=true;
            int palabra1=0,palabra2=0;
            while(palabra1<numeropalabras1 and palabra2<numeropalabras2){
                while(texto1[palabra1][posicion]!='\0' and texto2[palabra2][posicion]!='\0' and texto1[palabra1][posicion]!=' '  and texto2[palabra2][posicion]!=' '){
                if(texto1[palabra1][posicion]!=texto2[palabra2][posicion]){
                    igualdad=false;
                }
                posicion++;
                }
                posicion=0;
                if(igualdad==false){
                    ascii1=1;
                    while(texto1[palabra1][posicion]!='\0' and texto1[palabra1][posicion]!=' ' and ascii1!=-1){
                         cout<<texto1[palabra1][posicion];
                         ascii1=(int)texto1[palabra1][posicion];
                         posicion++;
                    }
                    posicion=0;
                    cout<<"  es diferente a  ";
                    ascii2=1;
                    while(texto2[palabra2][posicion]!='\0' and texto2[palabra2][posicion]!=' ' and ascii2!=-1){
                         cout<<texto2[palabra2][posicion];
                         ascii2=(int)texto2[palabra2][posicion];
                         posicion++;
                    }
                    cout<<endl;
                }
                palabra1++;
                palabra2++;
            }
            posicion=0;
}
        cout<<"el documento 1 tiene :"<<numeropalabras1<<" palabras"<<endl;
        cout<<"el documento 2 tiene :"<<numeropalabras2<<" palabras"<<endl;
}
        else cout<<"no se pudo abrir uno de los dos archivos"<<endl;

        archivo1.close();
        archivo2.close();
}
void contar_palabras(const char* name1,const char*name2,int*num1,int*num2){
    ifstream archivo1;
    ifstream archivo2;
    archivo1.open(name1,ios::in);
    archivo2.open(name2,ios::in);
    char texto1[1000],texto2[1000];
    int posicion=0, ascii1,ascii2,palabras1=0,palabras2=0;
    while(archivo1.good()){
        do{
             texto1[posicion]=archivo1.get();
             ascii1=(int)texto1[posicion];
             posicion++;
        }while (( ascii1!=32) and (ascii1!=-1) );
        palabras1++;
    }
    while(archivo2.good()){
        do{
             texto2[posicion]=archivo2.get();
             ascii2=(int)texto2[posicion];
             posicion++;
        }while (( ascii2!=32) and (ascii2!=-1) );
        palabras2++;
    }
    *num1=palabras1;
    *num2=palabras2;
}

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    milibreria.cpp \
    pract4.cpp

HEADERS += \
    milibreria.h \
    pract4.h

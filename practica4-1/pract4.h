#ifndef PRACT4_H
#define PRACT4_H


#include <map>

using namespace std;
class redes
{
public:
    redes();
    map<char,map<char,int>> nodos;
    void calcula_mejor_ruta();


};
class enrutador:public redes
{
private:
    char name;
public:
    enrutador();
    char get_name();
    void set_name();
    map<char,int> nodo;
   void enlaces_directos();


};

#endif // PRACT4_H

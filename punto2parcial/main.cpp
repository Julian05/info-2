#include <iostream>
#include <fstream>
void leer(const char*, const char*);
void contar_palabras(const char*,int*);
int contar_vocales(char*);

using namespace std;

int main()
{
    char nombrearchivo1[30],nombrearchivo2[30];
    const char *nombre1=nombrearchivo1,*nombre2=nombrearchivo2;
    cout << "Escriba el nombre del primer archivo a comparar palabra a palabra:  " << endl;
    cin.getline(nombrearchivo1,30);
    cout << "Escriba el nombre del 2 archivo a comparar palabra a palabra:  " << endl;
    cin.getline(nombrearchivo2,30);
    leer(nombre1,nombre2);

    return 0;
}
void leer(const char*nombre1,const char*nombre2){
        ifstream archivo1;
        archivo1.open(nombre1,ios::in);
        int numeropalabras1,posicion=0, ascii1;
        contar_palabras(nombre1,&numeropalabras1);
        char texto1[numeropalabras1][100];
        bool igualdad=true;
        if(archivo1.is_open()){
        while(archivo1.good()){
            for(int veces=0;veces<numeropalabras1;veces++){
            do{
                 texto1[veces][posicion]=archivo1.get();
                 ascii1=(int)texto1[veces][posicion];
                 posicion++;
            }while (( ascii1!=32) and ( ascii1!=-1) );
                posicion=0;
            }
            archivo1.close();
            posicion=0;
            igualdad=true;
            char resultado[numeropalabras1][50];
            int numvocales=0,numvocalessiguiente=0,posresultado=0;
                for(int veces=0;veces<numeropalabras1;veces++){

                    char *palabra=&texto1[veces][posicion],*palabrasiguiente=&texto1[veces+1][posicion];

                    numvocales=contar_vocales(palabra);
                    numvocalessiguiente=contar_vocales(palabrasiguiente);
                    if(numvocales<numvocalessiguiente)
                    {
                        posresultado=0;
                        while((palabra[posresultado]!='\0') and ( palabra[posresultado]!=' ')){
                        resultado[veces][posresultado]=palabra[posresultado];
                        posresultado++;
                        }
                    }
                    if(numvocales>numvocalessiguiente)
                    {
                        posresultado=0;
                        while((palabrasiguiente[posresultado]!='\0') and ( palabrasiguiente[posresultado]!=' ')){
                        resultado[veces][posresultado]=palabrasiguiente[posresultado];
                        posresultado++;
                        }
                    }
                }
                ofstream final;
                final.open(nombre2,ios::out);
                for(int veces=0;veces<numeropalabras1;veces++){
                    for(int pos=0;pos<50;pos++){
                        if((resultado[veces][pos]!='\0') and ( resultado[veces][posresultado]!=' ' )){
                                final<<resultado[veces][pos];
                    }


                    }


                }
                final.close();
}
        cout<<"el documento 1 tiene :"<<numeropalabras1<<" palabras"<<endl;
}
        else cout<<"no se pudo abrir uno de los dos archivos"<<endl;

        archivo1.close();
}
void contar_palabras(const char* name1,int*num1){
    ifstream archivo1;
    ifstream archivo2;
    archivo1.open(name1,ios::in);
    char texto1[1000];
    int posicion=0, ascii1,palabras1=0;
    while(archivo1.good()){
        do{
             texto1[posicion]=archivo1.get();
             ascii1=(int)texto1[posicion];
             posicion++;
        }while (( ascii1!=32) and (ascii1!=-1) );
        palabras1++;
    }

    *num1=palabras1;
}
int contar_vocales(char* palabra){
    int posicion=0, ascii1=1,contadorevocales=0;
    while(palabra[posicion]!='\0' and palabra[posicion]!=' ' and ascii1!=-1){
         ascii1=(int)palabra[posicion];
         if((ascii1=='a')|| (ascii1=='A') || (ascii1=='e') || (ascii1=='E') || (ascii1=='i') || (ascii1=='I') || (ascii1=='o') || (ascii1=='O') || (ascii1=='u') || (ascii1=='U')){
             contadorevocales++;
         }
         posicion++;
    }

}

#include <iostream>
#include<math.h>
/*programa que reciba un número n y lea una cadena de caracteres numéricos, el programa
debe separar la cadena de caracteres en números de n cifras, sumarlos e imprimir el resultado.*/

using namespace std;
void numero_cifras(int a,int *lon);/*cuenta el numero de cifras del numero ingresado*/
void pasar_a_arreglo(int ,int,int*,int sobra ,int);/*pasa el entero ingresado a una cadena de enteros*/

int main()
{
    int numero_entrada,veces,sobra=0,suma=0,posicion=0,dato_entrada,longitud=0,aux_arreglo=0;
    int* punteroarray;
    cout<<"ingrese un numero"<<endl;
    cin>>dato_entrada;
    cout<<"ingrese como quiere dividirlo"<<endl;
    cin>>numero_entrada;
    numero_cifras(dato_entrada, &longitud);/*se calcula el numero de cifras del numero ingresado*/
    if((longitud%numero_entrada)==0) veces=longitud/numero_entrada;/*se verifica si el numero de cifras y el numero para dividirlo es exacto */
    else{ /*si no se mira cuantas son exactas y cuantas quedan faltando*/
        veces=(longitud/numero_entrada)+1;
        sobra=longitud%numero_entrada;
    }
    if(sobra!=0)longitud+=(numero_entrada-sobra); /*si quedan sobran se aumenta su tamaño con 0 a la izq*/
    int arreglo_sumas[veces];
        int numerosalida[longitud];
        punteroarray=numerosalida;/*se señala el arreglo con un puntero para poder cambiarlo sin necesidad de un retorno*/
        pasar_a_arreglo(dato_entrada,longitud,punteroarray,sobra,numero_entrada);
    for(int j=0;j<veces;j++){
                aux_arreglo=0;
                for(int c=numero_entrada;c>0;c--){ /*se cuadran los emparejamientos de las sumas por posicion*/
                    aux_arreglo+=*(punteroarray+posicion)*(pow(10,(c-1)));

                posicion++;
                arreglo_sumas[j]=aux_arreglo;
                }

    }
    for(int repetir =0;repetir<veces;repetir++){ /*se suman los arreglos*/
        suma+= arreglo_sumas[repetir];
    }
    cout<<"original : "<<dato_entrada<<endl;
    cout<<"suma : "<<suma<<endl;


    return 0;
}
void numero_cifras(int a,int *lon){
    int digitos;
    *lon=0;
    digitos=a;
    while (digitos>0){ /*cuenta el numero de digitos del número ingresado*/
        digitos=digitos/10;
        *lon=*lon+1;
    }
}
void pasar_a_arreglo(int numero_entrada,int longitud,int* punteroarray,int sobra,int tamamo){
    int digitos=0,posicion=0;
    int aux;
    if(sobra!=0){
        for(int veces=0;veces<(tamamo-sobra);veces++){
        *(punteroarray+posicion)=0;
        posicion++;
        aux=(longitud-(tamamo-sobra));
        }
    }
    else aux=longitud;
    for(int veces= aux;veces>=1;veces--){ /* repite el ciclo dependiendo del numero de digitos*/
        digitos=numero_entrada/(pow(10,(veces-1))); /*se separa el numero por digitos*/
        *(punteroarray+posicion)=digitos ;
               posicion++;
        numero_entrada=numero_entrada-(digitos*(pow(10,(veces-1))));
    }
}

#include <iostream>
#include <math.h>

using namespace std;
/*función que reciba un numero entero y lo convierta a cadena de caracteres. Use
parámetros por referencia para retornar la cadena.*/
int validar_num();/*funcion que verifica si es dato ingresado es un numero*/
void convertir_int_a_char(int numero_entrada,int);/*fucion que recibe un entero y lo pasa a char*/
void numero_cifras(int numero_entrada,int *);/*funcion que cuenta el numero de cifras de iun entero*/
 void programa_de_prueba();/*programa que prueba el codigo*/


int main(){
    string eleccion;
    int numero_entrada,longitud=0;/*se inicializan las variables numero_entrada que va a contener el numero a convertir,longitud que va a tener su tamaño posteriormente y posicion*/
    cout<<"¿quiere implementar el programa de prueba? yes/no"<<endl;
    cin>>eleccion;
    if( eleccion == "yes" ) programa_de_prueba();

   else{
    cout<<"ingrese un numero"<<endl;
    numero_entrada=validar_num();
    numero_cifras(numero_entrada,&longitud);/* recibe la direcccion de longitud como parametro por referencia*/
    convertir_int_a_char(numero_entrada,longitud);
        }
    return 0;
}
int validar_num(){
    bool key=false;
    string num;
            while (key==false){
            cout<<""<<endl;
            cin >> num;
            if(num.length()==false) return  false;
            for(int i =0;i<num.length();i++){
                if(isdigit(num[i])) key=true;
                else {
                    key=false;
                    break;}
            }
            if(key==false) cout<<"intente de nuevo"<<endl;
}
            return atoi(num.c_str());

};
void numero_cifras(int numero_entrada,int *lon){
    int digitos;
    *lon=0;
    digitos=numero_entrada;
    while (digitos>0){ /*cuenta el numero de digitos del número ingresado*/
        digitos=digitos/10;
        *lon=*lon+1;
    }
}
void convertir_int_a_char(int numero_entrada,int longitud){
    int digitos,posicion=0;

        char numerosalida[longitud];
    for(int veces= longitud;veces>=1;veces--){ /* repite el ciclo dependiendo del numero de digitos*/
        digitos=numero_entrada/(pow(10,(veces-1))); /*se separa el numero por digitos*/
        numerosalida[posicion]=digitos +'0';/*convierte el entero a char*/
               posicion++;
        numero_entrada=numero_entrada-(digitos*(pow(10,(veces-1))));
    }
    posicion=0;
    for(int veces=longitud;veces>=1;veces--) {
        cout<<numerosalida[posicion];/*se imprime el entero ahora char*/
        posicion++;
    }
     cout<<"  en cadena de caracteres"<<endl;


}
void programa_de_prueba()
{
    int numero_entrada,lon=0;
    int prueba[]={123,1,22,100000};
    for(int repetir=0;repetir<4;repetir++){
    numero_entrada=prueba[repetir];
    numero_cifras(numero_entrada,&lon);/* recibe la direcccion de longitud como parametro por referencia*/
    convertir_int_a_char(numero_entrada,lon);
    }
}

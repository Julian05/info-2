#include "fisica.h"
#include <math.h>

fisica::fisica()
{
    ax=0;
    ay=0;
     masa=10;
     radio=12;
     x=20;
     y=10;
     e=0.5 ;
     k=0.009;
     vx=10;
     vy=10;
     v= sqrtf(pow(vx,2)+pow(vy,2));
     angulo=atan2(vy,vx);
     g=10;
    dt=0.1;
}

void fisica::actualizar_a(){
    ax=-(((k*v*pow(radio,2))/masa)*cos(angulo));
    ay=-(((k*v*pow(radio,2))/masa)*sin(angulo))-g;
}
void fisica::choque(){
        v=-e*v;
}
void fisica::actualizar_pos(){
    vx=vx+ax*dt;
    vy=vy+ay*dt;
    x=x+vx*dt+((ax*pow(dt,2))/2);
    y=y+vy*dt+((ay*pow(dt,2))/2);
}
void fisica::actualizar_angulo(){
    angulo=atan2(vy,vx);
}
void fisica::actualizar_v(){
    float aux_v=v;
    v= sqrtf(pow(vx,2)+pow(vy,2));
    e=-(v/aux_v);
}

float fisica::get_x(){
    return  x;
}
float fisica::get_y(){
    return  y;
}

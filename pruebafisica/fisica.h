#ifndef FISICA_H
#define FISICA_H


class fisica
{
private:
    float masa;
    float radio;
    float x;
    float y;
    float e;
    float k;
    float vx;
    float vy;
    float v;
    float angulo;
    float g;
    float dt;
    float ax;
    float ay;
    float PI= 3.14159265;
public:
    fisica();
    void actualizar_a();
    void choque();
    void actualizar_pos();
    void actualizar_v();
    void actualizar_angulo();
    float get_x();
    float get_y();


};

#endif // FISICA_H

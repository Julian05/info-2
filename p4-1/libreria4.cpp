#include "libreria4.h"
#include<iostream>
#include <list>
#include <fstream>
using namespace std;
string enrutador::get_name(int numlinea,string namear){ //con el txt se obtiene el nombre del nodo con ayuda del formato del txt
    ifstream archivo_redes;
    string linea,name;
    char caracter;
    int cont=0,poslinea=0;
    archivo_redes.open(namear,ios::in);
    while(archivo_redes.good()){
        while(poslinea<=numlinea){
                getline(archivo_redes,linea);

           poslinea++;
        };
    do{
       caracter=linea[cont];
       if(caracter!=':' and caracter!='\0'){
           name+=caracter;
           cont++;
       }
    }while(caracter!=':' and caracter!='\0');
    break;
    }
    archivo_redes.close();
    return name;
}
void redes::agregar_enrutador(string namear,list<string>&nombres_enrutadores){
    //se agrega al atributo nodos un nuevo nodo y sus enlaces si este no existia ya
    ofstream tabla;
    string nombre,enlace,newname;
    tabla.open(namear,ios::app);
    bool flat=true,verinodo,existencia;
        char eleccion;
        int costo,ascii=0;
        //tabla<<'\n';
        cout<<"Cual es el nombre de este enrutador?"<<endl;
        cin>>newname;
        newname=mayuscula(newname);
        existencia=verificar_nodo(nombres_enrutadores,newname);
        if(!existencia){
        //tabla<<newname<<':';
        list<string>::iterator itlist=nombres_enrutadores.end();
        nombres_enrutadores.insert(itlist,newname);
        while(flat){
            cout<<"con quien esta conectado directamente este nodo?(1 por 1)"<<endl;
            cin>>enlace;
            enlace=mayuscula(enlace);
            verinodo=verificar_nodo(nombres_enrutadores,enlace);
            if(verinodo){
            cout<<"que valor tiene?(1 por 1)"<<endl;
            cin>>costo;
            //tabla<<enlace<<'='<<costo;
            nodos[newname][enlace]=costo;
            nodos[enlace][newname]=costo;
            do{
            cout<<"Tiene mas enlaces?(y/n)"<<endl;
            cin>>eleccion;
            ascii=eleccion;
            if(ascii==78 || ascii==110){
                flat=0;
            }
            //else tabla<<',';
            }while(ascii!=89 and ascii!=121 and ascii!=78 and ascii!=110);
            ascii=0;
                   }
            else cout<<"la coneccion no existe actualmente"<<endl;

        }
        }
        else cout<<"El nodo ya existe"<<endl;
            tabla.close();
}
bool redes::verificar_nodo(list<string>nombres_nodos,string verificar){
    list<string>::iterator itlist;
    bool correcto=0;
    for(itlist=nombres_nodos.begin();itlist!=nombres_nodos.end();itlist++){
        if(*itlist==verificar){
            correcto=1;
            break;
        }
    }
    return  correcto;
}
void redes::eliminar_enrutador(string name_eliminar,list<string>&nombres){
   map<string,map<string,int>>::iterator it;
   map<string,int>::iterator prueba;
   list<string>::iterator itlist;
   for(it=nodos.begin();it!=nodos.end();it++){
           for(prueba=nodos[it->first].begin();prueba!=nodos[it->first].end();prueba++){
               if(prueba->first==name_eliminar){
                   nodos[prueba->first].erase(prueba);
                   break;
           }
       }
   }
   for(it=nodos.begin();it!=nodos.end();it++){
       if(it->first==name_eliminar){
           nodos.erase(it);
           break;
       }
   }
   for(itlist=nombres.begin();itlist!=nombres.end();itlist++){
       if(*itlist==name_eliminar){
           nombres.erase(itlist);
           break;
       }
   }
}
void redes::actualizar(string namear)
    {
               ofstream redes;
               redes.open(namear,ios::out);
               map<string,map<string,int>>::iterator itMayor;
               map<string,int>::iterator itMenor;
                map<string,int>::iterator itaux;
               for(itMayor=nodos.begin();itMayor!=nodos.end();itMayor++){
                   itMenor=nodos[itMayor->first].begin();
                   if(nodos[itMayor->first].size()!=0){
                       redes<<itMayor->first<<':';
                   }
                   for(itMenor=nodos[itMayor->first].begin();itMenor!=nodos[itMayor->first].end();itMenor++){
                       redes<<itMenor->first<<'='<<itMenor->second<<',';
                   }
                   if(nodos[itMayor->first].size()!=0 ){
                        redes<<'\n';
                   }
               }
           }

void redes::set_nodos(map<string,int>enrutador, string nombre){
    list<string>::iterator itlist;
        if(enrutador.size()>0){
     nodos[nombre]=enrutador;
        }

}
void redes::actualizar_tablamain(map<string,map<string,int>>&red,map<string,list<string>>&red2){
    red=nodos;
    red2=rutasD;
}
void redes::get_nodos(map<string,map<string,int>>&red,string nombrerouter ){
    if(!nombrerouter.empty()){
    red[nombrerouter]=nodos[nombrerouter];
    }
}
void redes::calcula_mejor_ruta(string inicio,string destino,list<string>posibles_rutas,bool imprimir){
    map<string,map<string,int>>::iterator itMayor;
    map<string,int>::iterator itMenor;
    map<string,int>::iterator iterador_aux;
    map<string,int>::iterator iterador_resul;
    map<string,int> mapa_aux;
    map<string,int> nodos_falantes;
    map<string,map<string,int>> final;
    map<string,map<string,int>> via_aux;
    via_aux=nodos;
    final=nodos;
     list<string> via;
     list<string> list_aux;
     list_aux=posibles_rutas;
     list<string>::iterator itlist;
    //se genera 2 mapas auxiliares con valores grandes,uno guardara los resultados finales y el otro ayudara a saber que nodos ya se miraron
    for(itlist=posibles_rutas.begin();itlist!=posibles_rutas.end();itlist++){
        mapa_aux[*itlist]=100000;
        nodos_falantes[*itlist]=100000;
    }
    //se elimina el nodo de inicio
    itMenor=nodos_falantes.find(inicio);
    nodos_falantes.erase(itMenor->first);
    int acumulado=0;
    mapa_aux[inicio]=0;
    itMayor=nodos.find(inicio);
    //se buscan los valores minimos
    for(itMenor=itMayor->second.begin();itMenor!=itMayor->second.end();itMenor++){
        iterador_aux=nodos_falantes.find(itMenor->first);
        iterador_resul=mapa_aux.find(itMenor->first);
        if(itMenor->second+acumulado<iterador_resul->second){
            iterador_resul->second=itMenor->second+acumulado;
            iterador_aux->second=itMenor->second+acumulado;
        }
    }
    itMayor=nodos.find(inicio);
    nodos.erase(itMayor->first);
    string nodoSiguiente;
    int numeroMenor;
    //se van recorriendo los diferentes caminos nodo por nodo y se compra cual es mas eficiente y se van eliminando los ya comparados
    while (!nodos.empty()) {
        numeroMenor=100000;
        for(itMenor=nodos_falantes.begin();itMenor!=nodos_falantes.end();itMenor++){
            if(itMenor->second<numeroMenor){
                numeroMenor=itMenor->second;
                iterador_aux=itMenor;
            }
        }
        acumulado=numeroMenor;
        nodoSiguiente=iterador_aux->first;;
        nodos_falantes.erase(iterador_aux->first);
        itMayor=nodos.find(nodoSiguiente);
        for(itMenor=itMayor->second.begin();itMenor!=itMayor->second.end();itMenor++){
            iterador_aux=nodos_falantes.find(itMenor->first);
            iterador_resul=mapa_aux.find(itMenor->first);
            if(itMenor->second+acumulado<iterador_resul->second){
                iterador_resul->second= itMenor->second+acumulado;
                iterador_aux->second= itMenor->second+acumulado;
            }
        }
        itMayor=nodos.find(nodoSiguiente);
        nodos.erase(itMayor->first);
    }


    final[inicio]=mapa_aux;
    //se le da el valor del resultado de la busqueda al atributo
    nodos=final;

    //ahora se mira cual fue la ruta que genero el menor costo
    string nodo=destino;
    int valor=nodos[inicio][destino];
    while(nodo!=inicio){
        for(itMenor=via_aux[nodo].begin();itMenor!=via_aux[nodo].end();itMenor++){
            if(nodos[inicio][itMenor->first]==(valor-via_aux[nodo][itMenor->first])){
                if(itMenor->first!=nodo){
                via.push_back(nodo);
                nodo=itMenor->first;
                valor=nodos[inicio][nodo];
                break;
             }
            }
        }
    }
    via.push_back(inicio);
    string nombre,clave;
    clave=inicio+"-"+destino;
    bool pertenece=0;

    //se mira si la ruta ya se habia generado antes para no sobre escribirla
    map<string,list<string>>::iterator itverificar;
    for(itverificar=rutasD.begin();itverificar!=rutasD.end();itverificar++){
        if(clave==itverificar->first){
            via=rutasD[clave];
            pertenece=1;
        }
    }
    rutasD[clave]=via;
    if(imprimir){
    cout<<"la ruta mas barata tiene un costo de : "<<nodos[inicio][destino]<<endl;
    cout<<"la ruta tomada fue :  ";
    while(!via.empty()){
        nombre=via.back();
        via.pop_back();
        cout<<nombre<<" ";
    }
    cout<<endl;
    }



}
void redes::tabla_rutas_cortas(list<string>posibles_rutas){ //genera la tabla con TODOS los minimos precios
    bool noimprimir=0;
    list<string>::iterator iterador1;
    list<string>::iterator iterador2;
    string inicio,destino;
    for(iterador1=posibles_rutas.begin();iterador1!=posibles_rutas.end();iterador1++){
        inicio=*iterador1;
        for(iterador2=posibles_rutas.begin();iterador2!=posibles_rutas.end();iterador2++){
            destino=*iterador2;
            calcula_mejor_ruta(inicio,destino,posibles_rutas,noimprimir);
        }
    }
}
void redes::mostrar_tabla(){ //muestra la tabla en pantalla

   map<string,map<string,int>>::iterator ittabla;
   map<string,int>::iterator itnodo;
   cout<<"\t";
   for(ittabla=nodos.begin();ittabla!=nodos.end();ittabla++){
       cout<<ittabla->first<<"\t";
   }
   cout<<endl;
   for(ittabla=nodos.begin();ittabla!=nodos.end();ittabla++){
        cout<<ittabla->first<<"\t";
       for(itnodo=nodos[ittabla->first].begin();itnodo!=nodos[ittabla->first].end();itnodo++)
       {
          cout<<itnodo->second<<"\t";
       }
       cout<<endl;
   }
}
void enrutador::enlaces_directos(int numlinea,string namear)
{//desde el txt se generan los enlaces del nodo
    char eleccion='k';
    string enlace,linea,auxcosto;
    int costo,numcaracteres,posicion=0,poslinea=0;
    ifstream archivo;
    archivo.open(namear,ios::in);
        while(poslinea<=numlinea){
                getline(archivo,linea);

           poslinea++;
        };
        numcaracteres=0;
        while (eleccion!='\0') {
            eleccion=linea[numcaracteres];
            numcaracteres++;
        }
        numcaracteres=numcaracteres-1;
        while (posicion<numcaracteres-1) {
            if((linea[posicion]==':' || (linea[(posicion)]==',' and linea[(posicion +1)]!='\0')) and linea!=""){
                posicion++;
                do {
                    eleccion=linea[posicion];
                    enlace+=eleccion;
                    posicion++;
                }while (linea[posicion]!='=');
                    posicion++;
                do {
                    eleccion=linea[posicion];
                    auxcosto+=eleccion;
                    posicion++;
                }while (linea[posicion]!=',' and linea[posicion]!='\0');
                costo=stoi(auxcosto);
                nodo[enlace]=costo;
            }
            auxcosto="";
            enlace="";
            eleccion=linea[(posicion)];
            if(linea[(posicion)]!=','){

                posicion++;
                eleccion=linea[(posicion)];
            }
        }
        archivo.close();

}
void enrutador::get_map(map<string,int>&main){
    map<string,int>::iterator it;
    for(it=nodo.begin();it!=nodo.end();it++){
        main[it->first]=nodo[it->first];
    }
    nodo.clear();
}
string redes::mayuscula(string name){
    string name2;
    for(int pos=0;pos<name.size();pos++){
        name2+=toupper(name[pos]);
    }
    return name2;
}

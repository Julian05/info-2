   #define LIBRERIA4_LIBRARY_H
#include <map>
#include <list>

using namespace std;
// se crean dos clases para diferenciar los dos objetos distintos pero se entrelazan entre ya que sus metodos van conjuntos
class redes
{
private:
        map<string,map<string,int>> nodos;//atributo donde se guardaran los nodos y sus enlaces
        map<string,list<string>> rutasD; //atributo que tendra los diferentes caminos a tomar
        string mayuscula(string name);
public:
   void set_nodos(map<string,int>,string );
   void get_nodos(map<string,map<string,int>>&mymapi,string );
   void actualizar(string namear); //actualiza la tabla conforme a los cambios
   void mostrar_tabla();
    void calcula_mejor_ruta(string inicio,string destino,list<string>posibles_rutas,bool imprimir=1);
    void agregar_enrutador(char,map<string,int> ,list<string>nombres_enrutadores);
    void agregar_enrutador(string namear,list<string>&nombres_enrutadores);
    void eliminar_enrutador(string,list<string>&);
    bool verificar_nodo(list<string>,string);//verifica si el nodo ya esta o no
    void tabla_rutas_cortas(list<string>posibles_rutas);
    void actualizar_tablamain(map<string,map<string,int>>&mymapi,map<string,list<string>>&red);//pasar los atributos al main



};
class enrutador:public redes
{
private:
    string name;
    void set_name();
    map<string,int> nodo;
public:
    string get_name(int,string namear );
   void enlaces_directos(int,string namear);
   void get_map(map<string,int>&mymap);


};

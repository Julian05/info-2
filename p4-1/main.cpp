
#include <iostream>
#include<map>
#include<list>
#include "libreria4.h"
#include <fstream>
#include<time.h>

using namespace std;
int contar_lineas(string);
string mayuscula(string);


int main()
{

    //en esta parte se genera un numero aleatorio para escoger uno de los tres archivos txt
    string name_archivo;
    srand(time(NULL));
    int archivo=1+rand()%(4-1);
    switch (archivo) {
    case 1 :
        name_archivo="redes.txt" ;
        break;
    case 2 :
        name_archivo="redes2.txt" ;
        break;
    case 3 :
        name_archivo="redes3.txt" ;
        break;

    }
    bool flat=1;
    string eleccion;
    string enrutador1,inicio,destino;
    int numlineas,linealeer=0;
    map<string,int>prueba;
    map<string,map<string,int>> redes;
    map<string,list<string>> caminos;
    list<string>nombres_enrutadores;
    enrutador red;//se crea un objeto de la clase enrutador

    //se lena el mapa de mapas con el txt
    numlineas=contar_lineas(name_archivo);
    while(linealeer<numlineas){
    enrutador1=red.get_name(linealeer,name_archivo);
    red.enlaces_directos(linealeer,name_archivo);
    red.get_map(prueba);
    list<string>::iterator itlist=nombres_enrutadores.begin();
    if(enrutador1.empty()==0){
        nombres_enrutadores.insert(itlist,enrutador1);
        itlist++;
    }
    red.set_nodos(prueba,enrutador1);
    prueba.clear();
    red.get_nodos(redes,enrutador1);
    linealeer++;

    }
    red.actualizar_tablamain(redes,caminos);

    //se preguntan los posibles cambios en la red
    bool repetir=1;
    string again;
    do{
    int ascii;
    bool nodo_correcto1=0,nodo_correcto2=0;
     while(flat){
    cout<<"quieres agregar mas enrutadores?(y/n)"<<endl;
    cin>>eleccion;
    if(eleccion.size()==1){
    ascii= (int)eleccion[0];
    }
    if(ascii==89 || ascii==121 || ascii==78 || ascii==110) flat=0;
    }
     if(ascii==89 || ascii==121){
         red.agregar_enrutador(name_archivo,nombres_enrutadores);
         red.actualizar(name_archivo);
     }
     flat=1;
     ascii=1000000000; //se coloca la variable ascii con un valor grande para que funcione correcta en la siguiente pregunta, y se pone flat como 1 para lo mismo
     while(flat){
     cout<<"quieres eliminar un enrutador?(y/n)"<<endl;
     cin>>eleccion;
     if(eleccion.size()==1){
        ascii= (int)eleccion[0];
     }
     if(ascii==89 || ascii==121 || ascii==78 || ascii==110) flat=0;
     }
      if(ascii==89 || ascii==121){
          string name_eliminar;
          cout<<"cual es el nombre del enrutador a eliminar "<<endl;
          cin>>name_eliminar;
         name_eliminar= mayuscula(name_eliminar);
         red.eliminar_enrutador(name_eliminar,nombres_enrutadores);
         red.actualizar(name_archivo);
      }
    flat=1;
    ascii=1000000000;
    while(flat){
    cout<<"quieres averiguar el camino más corto de una ruta?(y/n)"<<endl;
    cin>>eleccion;
    if(eleccion.size()==1){
    ascii= (int)eleccion[0];
    }
    if((ascii==89 || ascii==121 || ascii==78 || ascii==110 )) flat=0;
    }
    flat=1;

    if(ascii==89 || ascii==121){
        cout<<"cual es el punto de partida?"<<endl;
        cin>>inicio;
        inicio=mayuscula(inicio);
        nodo_correcto1= red.verificar_nodo(nombres_enrutadores,inicio);
        cout<<"cual es el punto de destino?"<<endl;
        cin>>destino;
        destino=mayuscula(destino);
        nodo_correcto2= red.verificar_nodo(nombres_enrutadores,destino);
        if(nodo_correcto1 and nodo_correcto2){
        red.calcula_mejor_ruta(inicio,destino,nombres_enrutadores);
        }
        else cout<<"el inicio o el destino no existen en la tabla de enrutamiento"<<endl;
    }
    flat=1;
    ascii=1000000000;

    red.tabla_rutas_cortas(nombres_enrutadores);
    red.actualizar(name_archivo);

     cout<<"la tabla con todas las rutas minimas queda así: "<<endl;
     red.mostrar_tabla();
     flat=1;
     ascii=1000000000;
     while(flat){
     cout<<"quieres seguir modificando la tabla?(y/n)"<<endl;
     cin>>again;
     if(again.size()==1){
     ascii= (int)again[0];
     }
     if(ascii==89 || ascii==121 || ascii==78 || ascii==110){
         flat=0;
         if(ascii==78 || ascii==110 )repetir=0;
     }
     }
     flat=1;

        }while(repetir);


}
int contar_lineas(string name){
    ifstream archivo_redes;
    string linea;
    int cont=0;
    cout<<"la tabla de nodos con sus rutas directas es la siguiente:  "<<endl;
    archivo_redes.open(name,ios::in);
    while(archivo_redes.good()){
    getline(archivo_redes,linea);
    cout<<linea<<endl;
    cont++;
    }
    archivo_redes.close();
    return cont;
}
string mayuscula(string name){
    string name2;
    for(int pos=0;pos<name.size();pos++){
        name2+=toupper(name[pos]);
    }
    return name2;
}

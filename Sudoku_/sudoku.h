#ifndef SUDOKU_H
#define SUDOKU_H
#include <vector>

typedef vector<int> fil;
typedef vector<fil> matriz;

class sudoku
{
private:
    int tam;

public:

    sudoku();
    int getTam() const;
    void setTam(int value);
    matriz m();

};

#endif // SUDOKU_H

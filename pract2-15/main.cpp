#include <iostream>

using namespace std;
void calcular_pos(int*);
void intercepcion(int*,int*,int*,bool*);
void ancho_alto (int*);

int main()
{
    bool bandera=false;/* bandera para verificar si hay intercepcion o no*/
    int rectangulo_a[4],rectangulo_b[4],rectangulo_c[4];//se van a empezar a pedir las diferentes coordenadas
    cout<<"dame el la posicion incial en x para el rectangulo a"<<endl;
    cin>>rectangulo_a[0];
    cout<<"dame el la posicion incial en y para el rectangulo a"<<endl;
    cin>>rectangulo_a[1];
    cout<<"dame el ancho para el rectangulo a"<<endl;
    cin>>rectangulo_a[2];
    cout<<"dame el alto para el rectangulo a"<<endl;
    cin>>rectangulo_a[3];
    cout<<"dame el la posicion incial en x para el rectangulo b"<<endl;
    cin>>rectangulo_b[0];
    cout<<"dame el la posicion incial en y para el rectangulo b"<<endl;
    cin>>rectangulo_b[1];
    cout<<"dame el ancho para el rectangulo b"<<endl;
    cin>>rectangulo_b[2];
    cout<<"dame el alto para el rectangulo b"<<endl;
    cin>>rectangulo_b[3];
    calcular_pos(rectangulo_a);/*implementacion de las funciones para calcular las posiciones iniciales y finales*/
    calcular_pos(rectangulo_b);
    intercepcion(rectangulo_a,rectangulo_b,rectangulo_c,&bandera);/*vine en formato (x0,y0,xf,yf)*/
    if(bandera){
    ancho_alto(rectangulo_c);
    cout<<"coordenadas de rectangulo interseccion"<<endl;
    cout<<"x1: "<<rectangulo_c[0]<<" y1:"<<rectangulo_c[1]<<endl;
    cout<<" ancho del rectangulo interseccion : "<<rectangulo_c[2]<<";alto del rectangulo interseccion :"<<rectangulo_c[3]<<endl;
}
else cout<<"no se interceptan en ningun punto}"<<endl;



}
void ancho_alto(int*rectac){
    if(rectac[0]>=0 and rectac[1]>=0){ /* primer cuadrante , parte primero y parte en cuarto*/
        rectac[2]=abs(rectac[0]-rectac[2]);/*calcular el ancho x inicial - x final de c - con valor absoluto*/
        rectac[3]=abs(rectac[1]-rectac[3]);/*calcular la altura y inicial - y final de c - con valor absoluto*/

    }
    if (rectac[0]>=0 and rectac[1]<0) {//cuarto cuadrante
        rectac[2]=abs(rectac[0]-rectac[2]);/*calcular el ancho x inicial - x final de c - con valor absoluto*/
        rectac[3]=abs(abs(rectac[1])-abs(rectac[3]));/*calcular la altura y inicial - y final de c - con valor absoluto*/

    }
    if(rectac[0]<0 and rectac[1]>=0){ /*segundo cuadrante*/
        rectac[2]=abs(abs(rectac[0])-abs(rectac[2]));/*calcular el ancho x inicial - x final de c - con valor absoluto*/
        rectac[3]=abs(rectac[1]-rectac[3]);/*calcular la altura y inicial - y final de c - con valor absoluto*/

    }
    if (rectac[0]<0 and rectac[1]<0) {/*tercer cuadrante*/
        cout<<rectac[0]<<"   "<<rectac[2]<<endl;
        rectac[2]=abs(abs(rectac[0])-abs(rectac[2]));/*calcular el ancho x inicial - x final de c - con valor absoluto*/
        rectac[3]=abs(abs(rectac[3])-abs(rectac[1]));/*calcular la altura y inicial - y final de c - con valor absoluto*/

    }

}

void calcular_pos(int*rectangulo){
    rectangulo[0]=*(rectangulo+0);//x inicial
    rectangulo[1]=*(rectangulo+1);//y inicial
    if(rectangulo[0]>=0 and rectangulo[1]>=0){ /* primer cuadrante , parte primero y parte en cuarto*/
    rectangulo[2]=abs(*(rectangulo+0)+*(rectangulo+2));// x final
    rectangulo[3]=abs(*(rectangulo+1)-*(rectangulo+3));//y final
    }
    if (rectangulo[0]>=0 and rectangulo[1]<=0) {//cuarto cuadrante
        if((rectangulo[0]==0 and rectangulo[1]<0 ) || (rectangulo[1]==0 and rectangulo[0]>0)||(rectangulo[1]>0 and rectangulo[0]<0)){
        rectangulo[2]=abs(*(rectangulo+0)+*(rectangulo+2));// x final
        rectangulo[3]=*(rectangulo+1)-*(rectangulo+3);//y final
        }
    }
    if(rectangulo[0]<=0 and rectangulo[1]>=0){ /*segundo cuadrante*/
        if((rectangulo[0]==0 and rectangulo[1]>0 ) || (rectangulo[1]==0 and rectangulo[0]<0)||(rectangulo[0]<0 and rectangulo[1]>0 )){
    rectangulo[2]=*(rectangulo+0)+*(rectangulo+2);// x final
    rectangulo[3]= abs(*(rectangulo+1)-*(rectangulo+3));//y final
        }
    }
    if (rectangulo[0]<=0 and rectangulo[1]<=0) {/*tercer cuadrante*/
        if((rectangulo[0]==0 and rectangulo[1]<0 ) || (rectangulo[1]==0 and rectangulo[0]<0)||(rectangulo[1]<0 and rectangulo[0]<0)){
        rectangulo[2]=*(rectangulo+0)+*(rectangulo+2);// x final
        rectangulo[3]=*(rectangulo+1)-*(rectangulo+3);//y final
        }
    }


}
void intercepcion(int*pos_a,int*pos_b,int*pos_c,bool* bandera){/*todas estos if lo que hacen es mirar con las coordenadas dadas por la funcion calcular pos mirar en que parte */
    if((pos_a[0]<=pos_b[0])and(pos_b[2]<=pos_a[2])){ /*1()1.1 y 1.2*/
        if(pos_a[1]<=pos_b[1]){
            if(pos_a[3]>=pos_b[3]and pos_a[1]>=pos_b[3]){
                pos_c[0]=pos_b[0];//x i
                pos_c[1]=pos_a[1];// y i
                pos_c[2]=pos_b[2];// x f
                pos_c[3]=pos_a[3];// y f es
                *bandera=true;
            }
            else{
                if(pos_a[3]<=pos_b[3]and pos_a[1]<=pos_b[3]){
                    pos_c[0]=pos_b[0];
                    pos_c[1]=pos_a[1];
                    pos_c[2]=pos_b[2];
                    pos_c[3]=pos_b[3];//es
                    *bandera=true;
                }
            }
        }

    }
        else{//lo de arriba bien creo, este es el mismo caso de arriba pero cambiando a por b
            if((pos_b[0]<=pos_a[0])and(pos_a[2]<=pos_b[2])){/*2, 2.1 y 2.2*/
                if(pos_b[1]<=pos_a[1]){
                    if(pos_b[3]>=pos_a[3]and pos_b[0]<=pos_a[3]){
                        pos_c[0]=pos_a[0];//x i
                        pos_c[1]=pos_b[1];// y i
                        pos_c[2]=pos_a[2];// x f
                        pos_c[3]=pos_b[3];// y f
                        *bandera=true;
                    }
                    else{
                        if(pos_b[3]<=pos_a[3] and pos_b[0]<=pos_a[3]){
                            pos_c[0]=pos_a[0];//x i
                            pos_c[1]=pos_b[1];// y i
                            pos_c[2]=pos_a[2];// x f
                            pos_c[3]=pos_a[3];// y f
                            *bandera=true;
                        }
                    }
                }
            }
    }
        if((pos_a[0]<=pos_b[0])and(pos_b[2]<=pos_a[2])){//3.1
            if(pos_a[1]>=pos_b[1]){
                if(pos_a[3]>=pos_b[3]and pos_a[1]>=pos_b[3]){
                    pos_c[0]=pos_b[0];//x i
                    pos_c[1]=pos_b[1];// y i
                    pos_c[2]=pos_b[2];// x f
                    pos_c[3]=pos_a[3];// y f
                    *bandera=true;
                }

            }
        }
        if((pos_b[0]<=pos_a[0])and(pos_a[2]<=pos_b[2])){//3.2
            if(pos_b[1]>=pos_a[1]){
                if(pos_b[3]>=pos_a[3]and pos_b[1]>=pos_a[3]){
                    pos_c[0]=pos_a[0];//x i
                    pos_c[1]=pos_a[1];// y i
                    pos_c[2]=pos_a[2];// x f
                    pos_c[3]=pos_b[3];// y f
                    *bandera=true;
                }

            }
        }
        if(pos_a[1]>=pos_b[1]) {//4,4.1,4,2
            if((pos_a[0]>=pos_b[0])and(pos_b[2]<=pos_a[2])){
                if(pos_a[3]>=pos_b[3]and pos_a[1]>pos_b[3]){
                    pos_c[0]=pos_a[0];//x i
                    pos_c[1]=pos_b[1];// y i
                    pos_c[2]=pos_b[2];// x f
                    pos_c[3]=pos_a[3];// y f
                    *bandera=true;

                }
            }
            if (((pos_a[0]<=pos_b[0])and(pos_b[2]>=pos_a[2]))) {
                if(pos_a[3]>=pos_b[3]and pos_a[2]>pos_b[0]){
                    pos_c[0]=pos_b[0];//x i
                    pos_c[1]=pos_b[1];// y i
                    pos_c[2]=pos_a[2];// x f
                    pos_c[3]=pos_a[3];// y f
                    *bandera=true;

                }

            }

        }/*5.1 y 5.2*/
        if(pos_b[1]>=pos_a[1]) {//4,4.1,4,2
            if((pos_b[0]>=pos_a[0])and(pos_a[2]<=pos_b[2])){
                if(pos_b[3]>=pos_a[3]and pos_b[1]>pos_a[3]){
                    pos_c[0]=pos_b[0];//x i
                    pos_c[1]=pos_a[1];// y i
                    pos_c[2]=pos_a[2];// x f
                    pos_c[3]=pos_b[3];// y f
                    *bandera=true;

                }
            }
            if (((pos_b[0]<=pos_a[0])and(pos_a[2]>=pos_b[2]))) {
                if(pos_b[3]>=pos_a[3]and pos_b[2]>pos_a[0]){
                    pos_c[0]=pos_a[0];//x i
                    pos_c[1]=pos_a[1];// y i
                    pos_c[2]=pos_b[2];// x f
                    pos_c[3]=pos_b[3];// y f
                    *bandera=true;

                }

            }

        }
        if(pos_a[0]>=pos_b[0] and pos_a[2]<=pos_b[2]) {//6,6.1,6,2
            if((pos_a[1]<=pos_b[1])and(pos_b[3]<=pos_a[3])){
                    pos_c[0]=pos_a[0];//x i
                    pos_c[1]=pos_a[1];// y i
                    pos_c[2]=pos_a[2];// x f
                    pos_c[3]=pos_a[3];// y f
                    *bandera=true;


            }
        }
            if (pos_b[0]>=pos_a[0] and pos_b[2]<=pos_a[2]) {
            if((pos_b[1]<=pos_a[1])and(pos_a[3]<=pos_b[3])){
                    pos_c[0]=pos_b[0];//x i
                    pos_c[1]=pos_b[1];// y i
                    pos_c[2]=pos_b[2];// x f
                    pos_c[3]=pos_b[3];// y f
                    *bandera=true;
            }

        }

            if(pos_a[0]<=pos_b[0] and pos_a[2]<=pos_b[2]){/*7.1 y 7.2*/
                if( pos_a[1]>=pos_b[1] and pos_a[3]<=pos_b[3]){
                    pos_c[0]=pos_b[0];//x i
                    pos_c[1]=pos_b[1];// y i
                    pos_c[2]=pos_a[2];// x f
                    pos_c[3]=pos_b[3];// y f
                    *bandera=true;

                }

            }
            if(pos_b[0]<=pos_a[0] and pos_b[1]>=pos_a[1]){/*7.1 y 7.2*/
                if(pos_b[2]<=pos_a[2] and pos_b[3]<=pos_a[3]){
                    pos_c[0]=pos_a[0];//x i
                    pos_c[1]=pos_a[1];// y i
                    pos_c[2]=pos_b[2];// x f
                    pos_c[3]=pos_a[3];// y f
                    *bandera=true;

                }
            }
            if(pos_a[0]>=pos_b[0] and pos_a[2]>=pos_b[2]){/*8.1 y 8.2*/
                if( pos_a[1]>=pos_b[1] and pos_a[3]<=pos_b[3]){
                    pos_c[0]=pos_a[0];//x i
                    pos_c[1]=pos_b[1];// y i
                    pos_c[2]=pos_b[2];// x f
                    pos_c[3]=pos_b[3];// y f
                    *bandera=true;

                }

            }
            if(pos_b[0]>=pos_a[0] and pos_b[1]>=pos_a[1]){/*8.1 y 8.2*/
                if(pos_b[2]>=pos_a[2] and pos_b[3]<=pos_a[3]){
                    pos_c[0]=pos_b[0];//x i
                    pos_c[1]=pos_a[1];// y i
                    pos_c[2]=pos_a[2];// x f
                    pos_c[3]=pos_a[3];// y f
                    *bandera=true;

                }
            }
}

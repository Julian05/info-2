 #include <iostream>
#include <fstream>
#include <cstring>

using namespace std;
void metodo(char*,char*);
void leer(const char*,const char*,char,int);
void escribir(int[8]);
void metodo1(const char*,int);
void metodo2 (const char*,int);
void binario_ascci(char,int*);
int contar_series(int limite);
int contar_letras();

int main()
{
    char nombrearchio_entrada[30], nombrearchio_salida[30],choose,bits;
    const char *entrada=nombrearchio_entrada,*salida=nombrearchio_salida;
    int limitebits;
    cout<<"escribe el nombre del archivo a codificar:  "<<endl;
    cin.getline(nombrearchio_entrada,30);
    cout<<"escribe el nombre del archivo de salida:  "<<endl;
    cin.getline(nombrearchio_salida,30);
    metodo(&choose,&bits);
    limitebits=bits-'0';
    leer(entrada,salida,choose,limitebits);

    return 0;
}

void metodo(char* punte1,char *punte2)
{
    char eleccion,eleccion2[3];
        bool ban=true, ban2=true;
        int ascci,cont=0;
        while(ban || ban2){
            cout << "Ingrese el tipo de metodo(1/2)" << endl;
            cin >> eleccion;
            cout << "Ingrese un numero n ara separar los bits" << endl;
            cin>>eleccion2;
      ascci=(int)eleccion;
      if(ascci>=49 and ascci<=50){
      ban=false;
      *punte1=eleccion;
      }
      else{
       cout<<"ingresa una opcion valida para el metodo"<<endl;
      }
      while(1){
          if (*(eleccion2+cont) == '\0')
              break;
              cont++;
  }
      for(int veces=0;veces<cont;veces++){
         if( isdigit(eleccion2[veces])){
             *(punte2+veces)=eleccion2[veces];
             ban2=false;
         }
         else ban2=true;
      }
      if(ban2==true)cout<<"ingresa una opcion valida para el numero de separacion"<<endl;
    }
    }
void leer(const char*nombreentrada,const char*nombresalida,char eleccion,int bitss){
        ifstream archivo;
        int code[8],*codificar=code;
        archivo.open(nombreentrada,ios::in);
        while(archivo.good()){
            char texto=archivo.get();
            binario_ascci(texto,codificar);
            int ascci=(int)texto;
            if(ascci<=255 and ascci>=1 ){
            escribir(code);
            }
        }
        archivo.close();
                if(eleccion=='1'){/*metodo 1*/
                    metodo1(nombresalida,bitss);
                }
                else if (eleccion=='2') {
                    metodo2(nombresalida,bitss);

                }

}
void binario_ascci(char caracter,int* codificacion)
{
    int ascci=(int)caracter,binario[8];
    if(ascci<=255 and ascci>=1 ){

    for(int bit=0;bit<8;bit++){
         if(ascci>3){
        binario[bit]=ascci%2;
        ascci=(ascci/2);
    }
    else{

            if(ascci==2 || ascci==3){
                binario[bit]=ascci%2;
                if(bit<7)bit++;
                binario[bit]=1;
                ascci=(ascci/2);
            }
            if(ascci==1){
               if(bit<7) bit++;
                binario[bit]=0;
            }
    }


    }
    int conta=0;
    for(int bit=7;bit>=0;bit--) {
        *(codificacion+bit)=binario[conta];
        conta++;
    }
    }

}

void escribir(int escribir[8]){
ofstream archivo;
archivo.open("codificar.txt",ios::app);
int aux;
for(int bit=0;bit<8;bit++) {
    aux=*(escribir+bit);
    archivo<<aux;
}
if(archivo.fail()){
    cout<<"no se pudo abrir el archivo"<<endl;
    exit(1);
}
archivo.close();

}
int contar_series( int limite){
    ifstream archivo;
    archivo.open("codificar.txt",ios::in);
    int series=0;
    while(archivo.good()){
        for(int veces=0;veces<limite;veces++){
            char caracter=archivo.get();
        }
        series++;
    }
    return series;
}
int contar_letras(){
    ifstream archivo;
    archivo.open("codificar.txt",ios::in);
    int series=0;
    while(archivo.good()){
        for(int veces=0;veces<8;veces++){
            char caracter=archivo.get();
        }
        series++;
    }
    series=series-1;
    return series;
}
void metodo1(const char* nombre,int limite){
    ifstream archivoleer;
    ofstream archivoescribir;
    int series=contar_series(limite),letras=contar_letras();
    int agreagar0s=0;
    if((limite%2)==0) {
        series=series-1;
        agreagar0s=0;
    }
    else{
        agreagar0s=abs((series*limite)-(letras*8));
    }
    archivoleer.open("codificar.txt",ios::in);
    archivoescribir.open(nombre,ios::out);
    char texto;
    for(int veces=0;veces<series;veces++){
        while(veces==0 and agreagar0s>0){
            archivoescribir<<0;
            agreagar0s--;
        }
        for(int bit=0;bit<limite;bit++) {
            archivoleer.get(texto);
            archivoescribir<<texto;         
        }
    }

    archivoescribir.close();
    archivoleer.close();
    ifstream codificar;
    codificar.open(nombre,ios::out);
    int num1s=0,num0s=0,caracter,particion[series][limite],llenado[series][limite];
    for(int veces=0;veces<series;veces++){
        for(int bit=0;bit<limite;bit++){
            char texto2=codificar.get();
             caracter=texto2-'0';
            particion[veces][bit]=caracter;
            llenado[veces][bit]=caracter;
        }
    }
    for(int veces=0;veces<series;veces++){
        num0s=0;
        num1s=0;
         for(int bit=0;bit<limite;bit++){
             if(veces==0){
                 if( particion[veces][bit]==1) {
                     num1s++;
                 }
                 if( particion[veces][bit]==0){
                     num0s++;
                 }
                 llenado[veces][bit]=!particion[veces][bit];
             }
             else{
                 if(particion[veces][bit]==1) {
                     num1s++;
                 }
                 if(particion[veces][bit]==0){
                     num0s++;
                 }
             }
         }
         if(veces !=(series-1)){
         if(num0s==num1s){
             for(int bit=0;bit<limite;bit++){
                llenado[veces+1][bit]=!particion[veces+1][bit];
         }

         }
         if(num0s>num1s){
             for(int bit=0;bit<limite;bit++){
                 bit=bit+1;
                    llenado[veces+1][bit]=!particion[veces+1][bit];
         }

         }
         if(num1s>num0s){
             for(int bit=0;bit<limite;bit++){
              bit=bit+2;
              if(bit<limite){
                    llenado[veces+1][bit]=!particion[veces+1][bit];
              }
         }
         }
         }


    }

    codificar.close();
    ofstream final;
    final.open(nombre,ios::out);
    cout<<"el mensage codificado con el metodo 1 es :";
    for(int veces=0;veces<series;veces++){
        for(int bit=0;bit<limite;bit++){
         final<< llenado[veces][bit];
         cout<< llenado[veces][bit];
        }
    }
    cout<<endl;
    final.close();

}

void metodo2(const char*nombre,int limite){
    ifstream archivoleer;
    ofstream archivoescribir;
    int series=contar_series(limite),letras=contar_letras();
    int agreagar0s=0;
    if((limite%2)==0) {
        series=series-1;
        agreagar0s=abs((series*limite)-(letras*8));
    }
    archivoleer.open("codificar.txt",ios::in);
    archivoescribir.open(nombre,ios::out);
    char texto;
    for(int veces=0;veces<series;veces++){
        while(veces==0 and agreagar0s>0){
            archivoescribir<<0;
            agreagar0s--;
        }
        for(int bit=0;bit<limite;bit++) {
            archivoleer.get(texto);
            archivoescribir<<texto;
        }
    }
    archivoescribir.close();
    archivoleer.close();
    ifstream codificar;
    codificar.open(nombre,ios::out);

    int caracter,particion[series][limite],llenado[series][limite];
    for(int veces=0;veces<series;veces++){
        for(int bit=0;bit<limite;bit++){
            char texto2=codificar.get();
             caracter=texto2-'0';
            particion[veces][bit]=caracter;
            llenado[veces][bit]=caracter;
        }
    }
    for(int veces=0;veces<series;veces++){
        for(int bit=0;bit<limite;bit++){
            if(bit==0){
                llenado[veces][bit]=particion[veces][limite-1 ];
        }
            else{
                llenado[veces][bit]=particion[veces][bit-1];
            }
    }
        }
    codificar.close();
    ofstream final;
    final.open(nombre,ios::out);
     cout<<"el mensage codificado con el metodo 2 es :";
    for(int veces=0;veces<series;veces++){
        for(int bit=0;bit<limite;bit++){
         final<< llenado[veces][bit];
         cout<< llenado[veces][bit];
        }
    }
    cout<<endl;
    final.close();

}




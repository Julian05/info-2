#include <iostream>
#include <cuerpofg.h>
#include <list>
#include <vector>
#include <fstream>

using namespace std;
float  validar_num();

int main()
{
    vector<Cuerpofg> cuerpos;
    list<float> posicionesx;
   list<float>::iterator itposx;
   list<float> posicionesy;
   list<float>::iterator itposy;
   list<float> posicionesx_aux;
   list<float> posicionesy_aux;
   list<float> masas;
   list<float>::iterator itmasas;
    int ascii=0;
    float x,y,m,vlx,vly;
    string eleccion,name,verificar;
    bool repetir=1,flat=1;

    // un loop para pedir los cuerpos  que tendra el sistema en cuestion
    do{
        x=0;
        y=0;
        cout<<"ingresa el nombre del cuerpo"<<endl;
        cin>>name;
        cout<<"ingresa la posicion inicial en x"<<endl;
        x=validar_num();
        posicionesx.push_back(x);
        cout<<"ingresa la posicion inicial en y"<<endl;
        y=validar_num();
        posicionesy.push_back(y);
        cout<<"ingresa la masa del cuerpo"<<endl;
        m=validar_num();
        masas.push_back(m);
        cout<<"ingresa la velocidad inicial en x"<<endl;
        vlx=validar_num();
        cout<<"ingresa la velocidad inicial en y"<<endl;
        vly=validar_num();
        Cuerpofg name(x,y,m,vlx,vly);
        cuerpos.push_back(name);
        system("CLS");//limpiar pantalla
        while(flat){
            cout<<"quieres seguir ingresando cuerpos(y/n)?"<<endl;
            cin>>eleccion;
        if(eleccion.size()==1){
        ascii= (int)eleccion[0];
        }
        if(ascii==89 || ascii==121 || ascii==78 || ascii==110){
            flat=0;
            if(ascii==78 || ascii==110 )repetir=0;
        }
        }
        flat=1;
    }while(repetir);

    ofstream documento;
    documento.open("SistemaGravitacional.txt",ios::out);

    //ahora ya con todos los cuerpos del sistema ,se calcularan sus interacciones
    int t=0;
    int conta;
    for(int s=0;s!=10000;s++){
        conta=0;
        for(int itcuerpos=0;itcuerpos!=cuerpos.size();itcuerpos++){
           cuerpos[itcuerpos].actualizarangulo(posicionesx,posicionesy,conta);
           cuerpos[itcuerpos].actualizarr(posicionesx,posicionesy,conta);
           cuerpos[itcuerpos].actualizarFg(masas,conta);
           cuerpos[itcuerpos].actualizarpos(t);
           cuerpos[itcuerpos].setpos(&x,&y);
           posicionesx_aux.push_back(x);
           posicionesy_aux.push_back(y);
           documento<<x<<"\t"<<y<<"\t";
           conta++;
        }
        if(s==0) t=1;
         documento<<'\n';
         posicionesx.clear();
         posicionesy.clear();
         posicionesx=posicionesx_aux;
         posicionesy=posicionesy_aux;
         posicionesx_aux.clear();
         posicionesy_aux.clear();

    }





    return 0;
}
float validar_num(){
    bool key=false;
    string num;
            while (key==false){
            cout<<""<<endl;
            cin >> num;
            if(num.length()==false) return  false;
            for(int i =0;i<num.length();i++){
                if(isdigit(num[i]) || num[i]=='.' || num[i]=='-') key=true;
                else {
                    key=false;
                    break;}
            }
            if(key==false) cout<<"intente de nuevo"<<endl;
}
            return stof(num.c_str());

}

#ifndef CUERPOFG_H
#define CUERPOFG_H
#include <list>
using namespace std;

class Cuerpofg
{
private:
    float posx;
    float posy;
    list<float> angulo;
    float masa;
    float Fgx;
    float Fgy;
    float vx;
    float vy;
    list<float> R;
    float G=1;
public:
    Cuerpofg(float px,float py,float m,float velx,float vely);
    void actualizarangulo(list<float> x2,list<float> y2,int conta);
    void actualizarr(list<float>posicionesx2,list<float>posicionesy2,int conta);
    void actualizarFg(list<float>masas,int conta);
    void actualizarpos(int t);
    void setpos(float*,float*);
    void pasar_aarchivo();


};

#endif // CUERPOFG_H

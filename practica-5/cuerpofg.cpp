#include "cuerpofg.h"
#include <math.h>
#include <list>
#include <fstream>
#include <stdlib.h>
#define PI 3.14159265
using namespace std;



Cuerpofg::Cuerpofg(float px,float py,float m,float velx,float vely){
    posx=px;
    posy=py;
    masa=m;
    vx=velx;
    vy=vely;
    Fgx=0;
    Fgy=0;
}

void Cuerpofg::actualizarangulo(list<float> x2,list<float> y2 , int m){

    list<float>::iterator itx2;

    list<float>::iterator ity2;

    float temp;

    for(itx2=x2.begin(),ity2=y2.begin();itx2!=x2.end() and ity2!=y2.end();itx2++,ity2++){


           if(*ity2!=posy || *itx2!=posx){
           temp=atan2((*ity2-posy),(*itx2-posx));
           //temp= (atan((*ity2-posy)/(*itx2-posx)));

           angulo.push_back(temp);

            }

      }

}

void Cuerpofg::actualizarr(list<float>posicionesx2,list<float>posicionesy2,int conta){
    list<float>::iterator itx2;
    list<float>::iterator ity2;
    float r,a;
    int cont=0;
        for(itx2=posicionesx2.begin() , ity2=posicionesy2.begin();itx2!=posicionesx2.end() and ity2!=posicionesy2.end();itx2++, ity2++){
                    if(conta!=cont){
                    r=sqrt(((pow((*itx2-posx),2))+ (pow((*ity2-posy),2))));
                    R.push_back(r);
                    }
                    cont++;
            }
}
void Cuerpofg::actualizarFg(list<float> m,int cont){
   list<float>::iterator itm;
   list<float>::iterator itR;
   list<float>::iterator itang;
    int conta=0;
    float tempx=0,tempy=0,fuerza;
    itang=angulo.begin();
    itR=R.begin();
    for(itm=m.begin();itm!=m.end();itm++){
        if( (conta!=cont) and itang!=angulo.end() and itR!=R.end()){
                fuerza=((G* *itm)/pow(*itR,2));
                tempx+=((G* *itm)/pow(*itR,2))* cos(*itang);
                tempy+=((G* *itm)/pow(*itR,2))* sin(*itang);
                itang++ ;
                itR++;
        }
        conta++;
    }
    Fgx=tempx;
    Fgy=tempy;

}
void Cuerpofg::actualizarpos(int t){
    posx= (posx+ vx*t +(Fgx * pow(t,2))/2);
    posy= (posy+ vy*t +(Fgy * pow(t,2))/2);
    vx= vx+ Fgx*t;
    vy= vy + Fgy*t;
    angulo.clear();
    R.clear();

}
void Cuerpofg::setpos(float*px,float*py){
    *px=posx;
    *py=posy;
}

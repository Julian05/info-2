#include "cuerpo.h"

cuerpo::cuerpo(float px, float py,float vx,float vy,float g)
{
    posx=px;
    posy=py;
    velx=vx;
    vely=vy;
    G=g;
}

void cuerpo::actualizar(float dt){
    posx=posx+velx*dt;
    posy=posy+vely*dt+ G*(dt*dt)/2;
    vely=vely+G*dt;
}

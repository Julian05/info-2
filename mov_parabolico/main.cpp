#include <iostream>
#include    <fstream>
#include <cuerpo.h>

using namespace std;

int main()
{
    ofstream archivo;
    archivo.open("mov_parabolico.txt",ios::out);

    cuerpo Borrador(0,1.7,7.07,7.07);
    float delt=0.1,y_actual;
    while(y_actual>0 ){
        archivo<<Borrador.get_posx()<<"\t"<<Borrador.get_posy()<<'\n';
        Borrador.actualizar(delt);
        y_actual=Borrador.get_posy();
    }
    archivo.close();
    return 0;
}

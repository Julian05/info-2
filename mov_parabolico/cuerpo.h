#ifndef CUERPO_H
#define CUERPO_H


class cuerpo
{
private:
    float G;
    float posx;
    float posy;
    float velx;
    float vely;

public:
    cuerpo(float px=0, float py=0,float vx=0,float vy=0,float g=-9.8);
    void actualizar(float dt);
    float get_posx() { return posx;};
    float get_posy(){return  posy;};

};

#endif // CUERPO_H

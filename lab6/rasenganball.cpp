#include "rasenganball.h"

rasenganball::rasenganball()
{
    angle=(qrand()%360);
    setRotation(angle);
    speed=5;
    //random start pos

    int startx=0;
    int starty=0;
    if((qrand())%1){
        startx=(qrand()%200);
        starty=(qrand()%200);
    }
    else{
        startx=(qrand()%-100);
        starty=(qrand()%-100);
    }
    setPos(mapToParent(startx,starty));


}

QRectF rasenganball::boundingRect() const
{
    return QRect(0,0,20,20);
}


void rasenganball::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QRectF rec=boundingRect();
    QBrush Brush(Qt::green);

    if(scene()->collidingItems(this).isEmpty()){
        Brush.setColor(Qt::gray);
    }
    else
    {
        Brush.setColor(Qt::red);
        DoCollision();
    }

    painter->fillRect(rec,Brush);
    painter->drawRect(rec);
}

void rasenganball::advance(int phase)
{
    if(!phase) return;
    QPointF location= this->pos();

    setPos(mapToParent(0,-(speed)));
}

void rasenganball::DoCollision()
{
    speed=0;
}

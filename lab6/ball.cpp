#include "ball.h"
#include <math.h>

rasengan::rasengan()
{
    ax=0;
    ay=0;
     masa=50.6;
     radio=5.3;
     x=20;
     y=10;
     e=0.51 ;
     k=(0.01 + (PI*pow(radio,2)));
     vx=100;
     vy=100;
     v= sqrtf(pow(vx,2)+pow(vy,2));
     angulo=atan2(vy,vx);
     g=10;
    dt=0.1;
}

void rasengan::actualizar_a(){
    ax=-(((k*v*pow(radio,2))/masa)*cos(angulo));
    ay=-(((k*v*pow(radio,2))/masa)*sin(angulo))-g;
}
void rasengan::choque(){
        v=-e*v;
}
void rasengan::actualizar_pos(){
    vx=vx+ax*dt;
    vy=vy+ay*dt;
    x=x+vx*dt+((ax*pow(dt,2))/2);
    y=y+vy*dt+((ay*pow(dt,2))/2);
}
void rasengan::actualizar_angulo(){
    angulo=atan2(vy,vx);
}
void rasengan::actualizar_v(){
    float aux_v=v;
    v= sqrtf(pow(vx,2)+pow(vy,2));
    e=-(v/aux_v);
}

float rasengan::get_x(){
    return  x;
}
float rasengan::get_y(){
    return  y;
}

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QTimer>
#include <QFile>
#include "grafic.h"
#include "ball.h"
#include <QKeyEvent>
#include <QFileDialog>
#include <QGraphicsView>
#include <QMainWindow>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    private slots:
    void keyPressEvent(QKeyEvent *event);


private:
    Ui::MainWindow *ui;
    int h_limit;                //longitud en X del mundo
    int v_limit;
    QGraphicsScene *scene;
    QGraphicsView *view;
    rasengangrafic *rass;
    QGraphicsLineItem* l1;
    QGraphicsLineItem* l2;
    QGraphicsLineItem* l3;
    QGraphicsLineItem* l4;
    void mousePressEvent(QMouseEvent *ev);
     QList<rasengangrafic*> rasengas;
};

#endif // MAINWINDOW_H

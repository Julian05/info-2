#ifndef RASENGANBALL_H
#define RASENGANBALL_H

#include<QPainter>
#include<QGraphicsItem>
#include <QGraphicsScene>


class rasenganball : public QGraphicsItem
{
public:
    rasenganball();
    QRectF boundingRect() const;
    void paint(QPainter *painter,const QStyleOptionGraphicsItem *option, QWidget *widget);

private:
    void advance(int phase);
    qreal angle;
    qreal speed;
    void DoCollision();




};

#endif // RASENGANBALL_H

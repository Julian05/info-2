#include "dialog.h"
#include "ui_dialog.h"
#include <rasenganball.h>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    scene= new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);
    ui->graphicsView->setRenderHint(QPainter::Antialiasing);
    scene->setSceneRect(0,0,1000,500);

    QPen mypen = QPen(Qt::red);
    QLineF topline(scene->sceneRect().topLeft(),scene->sceneRect().topRight());
    QLineF leftline(scene->sceneRect().topLeft(),scene->sceneRect().bottomLeft());
    QLineF rightline(scene->sceneRect().topRight(),scene->sceneRect().bottomRight());
    QLineF bottomline(scene->sceneRect().bottomLeft(),scene->sceneRect().bottomRight());
    scene->addLine(topline,mypen);
    scene->addLine(leftline,mypen);
    scene->addLine(rightline,mypen);
    scene->addLine(bottomline,mypen);

    int Itemcount=1;
    for(int i =0;i< Itemcount;i++){
        rasenganball *rasen = new rasenganball();
        scene->addItem(rasen);
    }

    timer= new QTimer(this);
    connect(timer,SIGNAL(TIMEOUT()),scene,SLOT(advance()));
    timer->start(100);
}

Dialog::~Dialog()
{
    delete ui;
}

#ifndef RASENGANGRAFIC_H
#define RASENGANGRAFIC_H
#include "ball.h"
#include <QPainter>
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>


class rasengangrafic:public QObject,
                     public QGraphicsPixmapItem
{
    Q_OBJECT

private:
    rasengan* ras;
public:
    rasengangrafic(QGraphicsItem *rass = 0);
    rasengan* get_rasengan();
    void fisica();
    void posicion(float v_limit);
    void posicion_par(float v_limit);
    ~rasengangrafic();
};

#endif // RASENGANGRAFIC_H

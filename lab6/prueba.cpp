#include "prueba.h"
#include "ui_mainwindow.h"
#include <QGraphicsView>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    h_limit=1000;                  //Asigna los valores leidos el archivo
    v_limit=500;
    scene=new QGraphicsScene(0,0, h_limit,v_limit);

    ui->View->setScene(scene);



    ui->View->setBackgroundBrush(QImage(":/new/prefix1/konoha.jpg"));

    l1=new QGraphicsLineItem(0,0,500,0);
    l2=new QGraphicsLineItem(0,0,0,400);
    l3=new QGraphicsLineItem(500,0,500,400);
    l4=new QGraphicsLineItem(0,400,500,400);
    scene->addItem(l1);
    scene->addItem(l2);
    scene->addItem(l3);
    scene->addItem(l4);
}
void MainWindow::mousePressEvent(QMouseEvent *ev)
{
    rasengas.append(new rasengangrafic());
    scene->addItem(rasengas.last());
    rasengas.last()->setPos(ev->x(),ev->y());
}

MainWindow::~MainWindow()
{
    delete ui;
}

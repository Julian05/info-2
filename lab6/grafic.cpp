#include "grafic.h"

rasengangrafic::rasengangrafic(QGraphicsItem* rass):QGraphicsPixmapItem(rass)
{
    setPixmap(QPixmap(":/new/prefix1/rasengan2.png"));
    ras= new  rasengan;
}
rasengan* rasengangrafic::get_rasengan(){
    return ras;
}

void rasengangrafic::fisica(){
    ras->actualizar_a();
    ras->actualizar_pos();
    ras->actualizar_v();
    ras->actualizar_angulo();

}
void rasengangrafic::posicion(float v_limit){
    setPos(ras->get_x(),v_limit-ras->get_y());
}
void rasengangrafic::posicion_par(float v_limit){
    setPos(ras->get_x(),v_limit-ras->get_y());
    ras->actualizar_pos();
}

rasengangrafic::~rasengangrafic(){
    delete ras;
}

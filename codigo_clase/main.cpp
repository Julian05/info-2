#include <iostream>
#include<stdio.h>

using namespace std;

int main()
{
    char A[2]={'A','a'};
    cout<<"cout: "<<A[0]<<A[1]<<endl;
    printf("printf:%c%c\n",A[0],A[1]);
    printf("printf:%d%d\n",A[0],A[1]);
    printf("printf:%x%x\n",A[0],A[1]);
    printf("printf:%x%x\n",(0xFF&~A[0]),(0xFF&~A[1]));

    for(int i=7;i>=0;i--){
        int bit=((A[0]&(1<<i))>>i);
        cout<<bit;
    }
    cout<<endl;
    return 0;
}
